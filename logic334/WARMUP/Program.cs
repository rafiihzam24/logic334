﻿using System;
using System.Runtime.InteropServices;

tugas5();

Console.ReadKey();

static void slowsorting()
{
    int[] arr = {1, 3, 10, 3, 5, 12314232};


    int x = 0;
    for (int i = 0; i < arr.Length; i++)
    {
        for (int j = 0; j < arr.Length - i - 1; j++)
        {
            if (arr[j] > arr[j + 1])
            {
                int tampung = arr[j];
                arr[j] = arr[j + 1];
                arr[j + 1] = tampung;
            }
        }
    }

    for (int i = 0;i < arr.Length; i++)
    {
        Console.Write($" {arr[i]} ");
    }
}



//SOAL 1

static void tugas1()
{
    Console.Write("Masukan Kalimat : ");
    string x = Console.ReadLine();

    char[] chars = x.ToCharArray();
    int hasil = 0;
    for (int i = 0;i < x.Length;i++)
    {
        if (chars[i] >= 'A' && chars[i] <= 'Z')
        {
            hasil++;
        }
    }

    Console.WriteLine(hasil);
}

static void tugas2()
{
    Console.Write("Masukan Start : ");
    int start = int.Parse(Console.ReadLine());
    Console.Write("Masukan End : ");
    int end = int.Parse(Console.ReadLine());

    string padkiri = string.Empty;


    for (int i = start; i <= end; i++)
    {
        padkiri = "XA-" + DateTime.Now.ToString("ddMMyyyy") + "-" + start.ToString().PadLeft(5, '0');
        Console.WriteLine(padkiri);
        start++;
    }
}


static void tugas3()
{
    Console.Write("Masukan Input : ");
    string input = Console.ReadLine();

    string x = input.Substring(0, (input.IndexOf(",")));
    int y = int.Parse(input.Substring(input.IndexOf(",") + 1));
    int z = 0;

    for (int i = 0; i < y; i++)
    {
        for (int j = 0; j < x.Length; j++)
        {
            z = z + int.Parse(x[j].ToString());
        }
    }

    string k = z.ToString();
    int l = 0;
    for (int i = 0;i < k.Length; i++)
    {
        l = l + int.Parse(k[i].ToString());
    }
    Console.WriteLine($"{z}");
    Console.WriteLine($"{l}");
}


static void tugas4()
{
    Console.Write("Masukan Isi Keranjang 1 : ");
    int x = int.Parse(Console.ReadLine());
    Console.Write("Masukan Isi Keranjang 2 : ");
    int y = int.Parse(Console.ReadLine());
    Console.Write("Masukan Isi Keranjang 3 : ");
    int z = int.Parse(Console.ReadLine());

    Console.Write("Keranjang mana yang diambil : ");
    int ambil = int.Parse(Console.ReadLine());

    if (ambil > 0 && ambil < 4)
    {
        switch (ambil)
        {
            case 1:
                Console.WriteLine($"Sisa Buah : {y+z}");
                break;
            case 2:
                Console.WriteLine($"Sisa Buah : {x+z}");
                break;
            case 3:
                Console.WriteLine($"Sisa Buah : {x+y}");
                break;
        }
    }
    else
    {
        Console.WriteLine("ERROR");
    }
}


static void tugas5()
{
    Console.WriteLine("-----PILIH DIBAWAH INI------");
    Console.WriteLine("MENU : ");
    Console.WriteLine("1. Laki Dewasa");
    Console.WriteLine("2. Wanita Dewasa");
    Console.WriteLine("3. Anak-Anak");
    Console.WriteLine("4. Bayi");

    bool stop = true;

    int lk = 0;
    int wd = 0;
    int wd2 = 0;
    int aa = 0;
    int b = 0;
    while (stop)
    {       
        Console.WriteLine("----------------------------");
        Console.Write("Masukan Nomor Untuk : ");
        int n = int.Parse(Console.ReadLine());
        Console.Write("Masukan Jumlah : ");
        int jumlah = int.Parse(Console.ReadLine());

        switch (n)
        {
            case 1:
                lk = lk + jumlah;
                Console.WriteLine();
                break;
            case 2:
                wd = wd + (jumlah * 2);
                wd2 = wd2 + jumlah;
                Console.WriteLine();
                break;
            case 3:
                aa = aa + (jumlah * 3);
                Console.WriteLine();
                break;
            case 4:
                b = b + (jumlah * 5);
                Console.WriteLine();
                break;
        }

        Console.Write("Apakah ingin Input Lagi (y/n): ");
        string x = Console.ReadLine().ToLower();

        if (x == "y")
        {
            continue;
        }
        else
        {
            stop = false;
        }

    }

    int hasil = lk + wd + aa + b;
    if (hasil % 2 == 1 && hasil > 10)
    {
        hasil = hasil + wd2;
    }
    Console.WriteLine();
    Console.WriteLine($"{hasil} Baju");
}


static void coba()
{

    List<int> list = new List<int>()
    {60, 2, 13, 24, 15, 6};


    Console.WriteLine(string.Join(" ", list.OrderBy(x => x)));
}



static void part01_3()
{
    Console.Write("Masukan n : ");
    int n = int.Parse(Console.ReadLine());

    int x = 0;

    if (n <= 100 && n >= 1)
    {
        for (int i = 0; i < n; i++)
        {
            if (i == 0)
            {
                Console.WriteLine("1" + x.ToString().PadLeft(2, '0') + $" adalah \"{"Si angka 1"}\"");
                x++;
            }
            else
            {
                x = x * 3;
                Console.WriteLine("1" + x.ToString().PadLeft(2, '0') + $" adalah \"{ "Si angka 1"}\"");
            }
            
        }
    }
    else
    {
        Console.WriteLine("ERROR");
    }
}


static void part01_2()
{
    Console.Write("Masukan Kalimat : ");
    string kal = Console.ReadLine().ToLower();

    List<char> list = new List<char>();
    List<char> list2 = new List<char>();

    for (int i = 0; i < kal.Length; i++)
    {
            if (kal[i] == 'a' || kal[i] == 'o' || kal[i] == 'e' || kal[i] == 'u' || kal[i] == 'i' )
            {
                list.Add(kal[i]);
            }
            else
            {
                list2.Add(kal[i]);
            }
    }

    Console.WriteLine($"Huruf Vokal : " + string.Join("", list.OrderBy(a => a)));
    Console.WriteLine($"Huruf Konsonan : " + string.Join("", list2.OrderBy(a => a)));
}

static void part01_4()
{
    Console.Write("Masukan Rute : ");
    string rute = Console.ReadLine().ToUpper();

    string[] rute2 = rute.ToString().Replace(" ", "").Split("-");
    double m = 0;

    for (int i = 0;i < rute2.Length - 1;i++)
    {
        if (rute2[i] == "TOKO")
        {
            switch (rute2[i+1])
            {
                case "TEMPAT1":
                    m += 2000;
                    break;
                case "TEMPAT2":
                    m += 2500;
                    break;
                case "TEMPAT3":
                    m += 4000;
                    break;
                case "TEMPAT4":
                    m += 6500;
                    break;
            }
        }
        else if (rute2[i] == "TEMPAT1")
        {
            switch (rute2[i + 1])
            {
                case "TOKO":
                    m += 2000;
                    break;
                case "TEMPAT2":
                    m += 500;
                    break;
                case "TEMPAT3":
                    m += 2000;
                    break;
                case "TEMPAT4":
                    m += 4500;
                    break;
            }
        }
        else if (rute2[i] == "TEMPAT2")
        {
            switch (rute2[i + 1])
            {
                case "TEMPAT1":
                    m += 500;
                    break;
                case "TOKO":
                    m += 2500;
                    break;
                case "TEMPAT3":
                    m += 1500;
                    break;
                case "TEMPAT4":
                    m += 4000;
                    break;
            }
        }
        else if (rute2[i] == "TEMPAT3")
        {
            switch (rute2[i + 1])
            {
                case "TEMPAT1":
                    m += 2000;
                    break;
                case "TOKO":
                    m += 4000;
                    break;
                case "TEMPAT2":
                    m += 1500;
                    break;
                case "TEMPAT4":
                    m += 2500;
                    break;
            }
        }
        else if (rute2[i] == "TEMPAT4")
        {
            switch (rute2[i + 1])
            {
                case "TEMPAT1":
                    m += 4500;
                    break;
                case "TOKO":
                    m += 6500;
                    break;
                case "TEMPAT3":
                    m += 2500;
                    break;
                case "TEMPAT2":
                    m += 4000;
                    break;
            }
        }

    }  
    Console.WriteLine($"{Math.Round(m/2500, 1)} Liter");
}


