﻿
Tugas3();

Console.ReadKey();

static void Tugas1()
{
    Console.WriteLine("---BERBAGAI CARA---");
    Console.Write("Berapa Anak Yang Duduk : ");
    int x = int.Parse(Console.ReadLine());

    int c = x;
    int y = 0;
    int hasil = 1;
    Console.Write($"{x}! : ");
    for (int i = 0; i < c; i++)
    {
        if (i == 0)
        {
            Console.Write($"{x}");
            y = x;
            x -= 1;
        }
        else
        {
            Console.Write($"x{x}");
            hasil = y * x;
            y = hasil;
            x -= 1;
        }
    }
    Console.WriteLine();

    Console.WriteLine($"Ada {hasil} Cara");
}


static void Tugas2()
{
    Console.WriteLine("---SOS---");
    Console.Write("Masukan Sinyal SOS : ");
    string x = Console.ReadLine().ToUpper();
    
    char[] toChar = x.ToCharArray();
    int y = 0;


    for (int i = 0; i < toChar.Length; i += 3)
    {
        if (toChar.Length % 3 == 0)
        {
            if (toChar[i] != 'S' || toChar[i + 1] != 'O' || toChar[i + 2] != 'S')
            {
                y++;
            }
            else
            {
                continue;
            }
        }
        else if (toChar.Length % 2 == 0) 
        {
            toChar = toChar.Concat(new char[] { 'x' }).ToArray();
            toChar = toChar.Concat(new char[] { 'r' }).ToArray();
            i = 0;
        }
        else
        {
            toChar = toChar.Concat(new char[] { 'r' }).ToArray();
            i = 0;
        }
    }
    Console.WriteLine(y) ;
}


static void Tugas3()
{
    Console.WriteLine("---DENDA---");
    Console.Write("Masukan Tanggal Meminjam Buku (dd-MM-yyyy) : ");
    string dt = Console.ReadLine();
    Console.Write("Masukan Tanggal Mengembalikan Buku (dd-MM-yyyy) : ");
    string dt2 = Console.ReadLine();

    try
    {
        DateTime date1 = DateTime.ParseExact(dt, "dd-MM-ss", null);
        DateTime date2 = DateTime.ParseExact(dt2, "dd-MM-yyyy", null);

        TimeSpan interval = date2 - date1;

        int x = interval.Days;
        decimal m = 0;

        if (x > 3)
        {
            m = (x - 3) * 500;
            Console.WriteLine($"Keterlambatan : {x-3} Hari");
            //Console.WriteLine($"Denda Yang Dibayarkan Adalah : {m.ToString("C2")}");
            Console.WriteLine($"Denda Yang Dibayarkan Adalah : Rp. {m.ToString("#,#.00")}");
        } 
        else if (x > 0 && x <= 3)
        {
            Console.WriteLine("Tidak Perlu Membayar Denda");
        }
        else
        {
            Console.WriteLine("Wrong Input");
        }
    }
    catch (Exception ex)
    {
        Console.WriteLine(ex.Message);
    }

}


static void Tugas4()
{
    Console.WriteLine("---BATCH 319---");
    Console.Write("Masukan Tanggal Mulai (dd/MM/yyyy) : ");
    string dt = Console.ReadLine();
    Console.Write("Masukan Hari Libur (,): ");
    string libur = Console.ReadLine();

    string[] libur2 = libur.Split(",");
    int[] numb = new int[libur2.Length];
    for (int i = 0; i < numb.Length; i++)
    {
        numb[i] = int.Parse(libur2[i]);
    }

    int x = 0;
    try
    {
        DateTime dt2 = DateTime.ParseExact(dt, "dd/MM/yyyy", null);
        for (int i = 1;i <= 10; i++)
        {
            if (dt2.DayOfWeek == DayOfWeek.Sunday || dt2.DayOfWeek == DayOfWeek.Saturday)
            {
                dt2 = dt2.AddDays(1);
                i -= 1;
            }
            else if (x < numb.Length && dt2.Day == numb[x])
            {
                dt2 = dt2.AddDays(1);
                i -= 1;
                x++;
            }
            else
            {
                dt2 = dt2.AddDays(1);
            }
        }
        Console.WriteLine($"Kelas Akan Ujian Pada : {dt2.ToString("dd/MM/yyyy")}");
    } 
    catch (Exception e) 
    {
        Console.WriteLine("Error");
        Console.WriteLine(e.Message);
    }
}



static void Tugas5()
{
    Console.WriteLine("---VOKAL KONSONAN---");
    Console.Write("Masukan Kalimat : ");
    string kalimat = Console.ReadLine().ToLower();

    char[] charArr = kalimat.ToCharArray();
    char[] vokal = { 'a', 'e', 'u', 'i', 'o' };

    int x = 0;
    int y = 0;
    for (int i = 0; i < charArr.Length; i++)
    {
        if (char.IsLetter(charArr[i])) {
            bool b = false;
            for (int j = 0; j < vokal.Length; j++)
            {
                if (charArr[i] == vokal[j])
                {
                    x++;
                    j = vokal.Length - 1;
                    b = true;
                }
                else if (charArr[i] == ' ')
                {
                    b = true;
                }
            }
            if (b == false)
            {
                y++;
            }
        }
    }

    Console.WriteLine($"Jumlah Huruf Vokal : {x}");
    Console.WriteLine($"Jumlah Huruf Konsonan : {y}");
}



static void Tugas6()
{
    Console.WriteLine("---***[CHAR]***---");
    Console.Write("Masukan Kata Atau Kalimat : ");
    string word = Console.ReadLine().ToLower();

    char[] charArr = word.ToCharArray();

    Console.WriteLine();
    for (int i = 0;i < charArr.Length;i++)
    {
        if (char.IsLetter(charArr[i])) {
            if (charArr[i] == ' ')
            {
                Console.WriteLine();
            }
            else
            {
                Console.WriteLine($"***{charArr[i]}***");
            }
        }
    }
}


static void Tugas7()
{
    Console.WriteLine("---MAKAN CUY---");
    Console.Write("Masukan Total Menu : ");
    int menu = int.Parse( Console.ReadLine() );
    int[] makan = new int[menu];
    for (int i = 0;i < menu;i++)
    {
        Console.Write($"Masukan Harga Menu ke-{i+1} : ");
        makan[i] = int.Parse(Console.ReadLine());
    }
    Console.Write("Index Makanan Alergi : ");
    int alergi = int.Parse( Console.ReadLine() );
    Console.Write("Masukan Uang : ");
    int uang = int.Parse( Console.ReadLine());

    int jumlah = 0;
    for (int i = 0; i < makan.Length;i++)
    {
        jumlah = jumlah + makan[i];
    }

    Console.WriteLine("------------------------------------") ;

    int real = 0;
    int bayar = 0;
    int sisa = 0;
    if (alergi <= makan.Length)
    {
        real = jumlah - makan[alergi];
        bayar = real/2;
        sisa = uang - bayar;
        switch (sisa)
        {
            case int when (sisa > 0):
                Console.WriteLine($"Elsa Harus Membayar : {bayar}");
                Console.WriteLine($"Sisa Uang Elsa : {sisa}");
                break;
            case int when (sisa < 0):
                Console.WriteLine($"Elsa Harus Membayar : {bayar}");
                Console.WriteLine($"Uang Elsa Kurang : {sisa}");
                break;
            case int when (sisa == 0):
                Console.WriteLine($"Elsa Harus Membayar : {bayar}");
                Console.WriteLine("Uang Elsa Pas");
                break;
        }
    }
    else
    {
        Console.WriteLine("Indeks Alergi Tidak Ditemukan, Maka :");
        bayar = jumlah / 2;
        sisa =  uang - bayar;
        switch (sisa)
        {
            case int when (sisa > 0):
                Console.WriteLine($"Elsa Harus Membayar : {bayar}");
                Console.WriteLine($"Sisa Uang Elsa : {sisa}");
                break;
            case int when (sisa < 0):
                Console.WriteLine($"Elsa Harus Membayar : {bayar}");
                Console.WriteLine($"Uang Elsa Kurang : {sisa}");
                break;
            case int when (sisa == 0):
                Console.WriteLine($"Elsa Harus Membayar : {bayar}");
                Console.WriteLine("Uang Elsa Pas");
                break;
        }
    }
}

static void Tugas8()
{
    Console.WriteLine("---SEGITIGA---");
    Console.Write("Masukan Input : ");
    int x = int.Parse(Console.ReadLine());


    for (int i = 1; i <= x; i++)
    {
        int y = 0;
        y = x - i;
        for (int j = 1; j <= x; j++)
        {
            if (j <= y)
            {
                Console.Write($" ");
            }
            else
            {
                Console.Write("*");
            }
        }
        Console.WriteLine();
    }
}


static void Tugas9()
{
    Console.WriteLine("---DIAGONALIZE---");
    int[] x = new int[3];
    int[] y = new int[3];
    int[] z = new int[3];
    for (int i = 0; i < x.Length; i++)
    {
        Console.Write($"Masukan Angka Baris 0 Kolom Ke-{i}: ");
        x[i] = int.Parse(Console.ReadLine());
    }
    for (int i = 0; i < y.Length; i++)
    {
        Console.Write($"Masukan Angka Baris 1 Kolom Ke-{i}: ");
        y[i] = int.Parse(Console.ReadLine());
    }
    for (int i = 0; i < z.Length; i++)
    {
        Console.Write($"Masukan Angka Baris 2 Kolom Ke-{i}: ");
        z[i] = int.Parse(Console.ReadLine());
    }

    int diag1 = 0;
    int diag2 = 0;
    for (int i = 0;i < x.Length; i++)
    {
        for (int j = 0; j <= y.Length; j++)
        {
            for(int k = 0; k <= z.Length; k++)
            {
                if (i == 0 && j == 1 && k == 2)
                {
                    diag1 = x[i] + y[j] + z[k];
                }
                else if (i == 2 && j == 1 && k == 0)
                {
                    diag2 = x[i] + y[j] + z[k];
                }
            }
        }
    }
    Console.WriteLine($"Perbedaan Diagonal = {diag1 - diag2}");
}



static void Tugas10()
{
    Console.WriteLine("---LILIN---");
    Console.Write("Masukan Tinggi Lilin Dengan ' ' : ");
    string y = Console.ReadLine();

    string[] tinggi = y.Split(' ');
    //int[] tinggi = Array.ConvertAll(y, int.Parse);
    string z = tinggi.Max();
    int k = 0;
    
    for (int i = 0; i < tinggi.Length; i++)
    {
        if (tinggi[i] == z)
        {
            k++;
        }
    }
    Console.WriteLine(k);
}
