﻿
Tugas8();

Console.ReadKey();

static void Tugas1()
{
    Console.WriteLine("---UPAH KARYAWAN---");
    Console.Write("Masukan Golongan Anda : ");
    int gol = int.Parse(Console.ReadLine());
    Console.Write("Masukan Jam Kerja Anda : ");
    int jk = int.Parse(Console.ReadLine());
    int upah = 0;
    double lembur = 0;

    if (gol >= 1 && gol <= 4)
    {
        if (jk <= 40)
        {
            switch (gol)
            {
                case int when (gol == 1):
                    upah = jk * 2000;
                    break;
                case int when (gol == 2):
                    upah = jk * 3000;
                    break;
                case int when (gol == 3):
                    upah = jk * 4000;
                    break;
                case int when (gol == 4):
                    upah = jk * 5000;
                    break;
            }
        }
        else
        {
            int jk2 = 0;
            switch (gol)
            {
                case int when (gol == 1):
                    jk2 = jk - 40;
                    upah = 40 * 2000;
                    lembur = jk2 * 3000;
                    break;
                case int when (gol == 2):
                    jk2 = jk - 40;
                    upah = 40 * 3000;
                    lembur = jk2 * 4500;
                    break;
                case int when (gol == 3):
                    jk2 = jk - 40;
                    upah = 40 * 4000;
                    lembur = jk2 * 6000;
                    break;
                case int when (gol == 4):
                    jk2 = jk - 40;
                    upah = 40 * 5000;
                    lembur = jk2 * 7500;
                    break;
            }
        }
        Console.WriteLine("---------------------------");
        Console.WriteLine($"Upah : {upah}");
        Console.WriteLine($"Lembur : {lembur}");
        Console.WriteLine($"Total : {upah + lembur}");
    }
    else
    {
        Console.WriteLine("Golongan Tidak Ditemukan");
    }
}



static void Tugas2()
{
    Console.WriteLine("---PEMBAGIAN KATA---");
    Console.Write("Masukan Kalimat : ");
    string kal = Console.ReadLine();

    string[] kalimat = kal.Split(' ');
    int n = 1;
    for (int i = 0; i < kalimat.Length; i++)
    {
        if (kalimat[i] == "")
        {
            continue;
        }
        Console.WriteLine($"Kata {n} = {kalimat[i]}");
        n++;
    }
    Console.WriteLine($"Total Kata Adalah : {n-1}");

}



static void Tugas3()
{
    Console.WriteLine("---SENSOR HURUF---");
    Console.Write("Masukan Kalimat : ");
    string kal = Console.ReadLine();
    string[] kalimat = kal.Split(" ");

    for (int i = 0;i < kalimat.Length;i++)
    {
        for (int j = 0; j < kalimat[i].Length; j++)
        {
            if (j != 0 && j != kalimat[i].Length - 1)
            {
                Console.Write("*");
            }
            else
            {
                Console.Write(kalimat[i][j]);
            }
        }
        Console.Write(" ");
    }
}



static void Tugas4()
{
    Console.WriteLine("---SENSOR HURUF 2---");
    Console.Write("Masukan Kalimat : ");
    string kal = Console.ReadLine();
    string[] kalimat = kal.Split(" ");

    for (int i = 0; i < kalimat.Length; i++)
    {
        for (int j = 0; j < kalimat[i].Length; j++)
        {
            if (j != 0 && j != kalimat[i].Length - 1)
            {
                Console.Write(kalimat[i][j]);
            }
            else
            {
                Console.Write("*");
            }
        }
        Console.Write(" ");
    }
}



static void Tugas5()
{
    Console.WriteLine("---SENSOR HURUF 3---");
    Console.Write("Masukan Kalimat : ");
    string kal = Console.ReadLine();
    string[] kalimat = kal.Split(" ");

    for (int i = 0;i < kalimat.Length;i++)
    {
        for (int j = 0; j < kalimat[i].Length; j++)
        {
            if (j != 0)
            {
                Console.Write(kalimat[i][j]);
            }
            else
            {
                Console.Write("");
            }
        }
        Console.Write(" ");
    }

}


static void Tugas6()
{
    Console.WriteLine("---PANGKAT 3---");
    Console.Write("Iterasi yang diinginkan : ");
    int n = int.Parse(Console.ReadLine());

    int x = 3;
    if (n != 0)
    {
        for (int i = 1; i <= n; i++)
        {
            if (i % 2 == 0)
            {
                Console.Write("  *  ");
                x = x * 3;
            }
            else
            {
                Console.Write($" {x} ");
                x = x * 3;
            }
        }
    }
    else
    {
        Console.WriteLine("Wrong Input");
    }
}



static void Tugas7()
{
    Console.WriteLine("---FIBONACCI---");
    Console.Write("Iterasi yang diinginkan : ");
    int n = int.Parse(Console.ReadLine());
    Console.Write("Masukan angka awal : ");
    int j = int.Parse(Console.ReadLine());

    int x = 0;
    int z = 0;
    int y = j;
    for (int i = 1;i <= n; i++)
    {
        Console.WriteLine(y);
        z = y;
        y = x + z;
        x = z;
    }
}



static void Tugas8()
{
    Console.WriteLine("---POLA DERET---");
    Console.Write("Masukan iterasi yang diinginkan : ");
    int n = int.Parse(Console.ReadLine());

    int x = n;
    for (int i = 1; i <= n ; i++)
    {
        for(int j = 1; j <= n ; j++)
        {
            if (i == 1)
            {
                Console.Write($" {j}");
            }
            else if (i == n && j == 1)
            {
                Console.Write($" {n}");
            }
            else if (i == n)
            {
                Console.Write($" {x = x - 1}");
            }
            else if (i > 1 && j == 1 || j == n)
            {
                Console.Write($" {"*"}");
            }
            else
            {
                Console.Write($" {" "}");
            }
        }
        Console.WriteLine();
    }
}