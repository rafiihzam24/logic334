﻿
using System.Diagnostics.Contracts;

Tugas4();

Console.ReadKey();

static void Tugas1()
{
    Console.WriteLine("---ITERASI---");
    Console.Write("Masukan Iterasi Yang Diinginkan : ");
    int n = int.Parse(Console.ReadLine());
    Console.Write("Masukkan Angka Awal Yang Ingin Diiterasi : ");
    int x = int.Parse(Console.ReadLine());
    Console.Write("Masukan Kelipatan : ");
    int y = int.Parse(Console.ReadLine());

    for (int i = 1; i < n; i++)
    {
        if (i % 2 == 1 && y < 0)
        {
            Console.Write($"{x} ");
            x = x + y;
        }
        else if (y < 0)
        {
            Console.Write($"{(-(x))} ");
            x = x + y;
        }

        if (i % 2 == 1 && y > 0)
        {
            Console.Write($"{(-(x))} ");
            x = x + y;
        }
        else if (y > 0)
        {
            Console.Write($"{x} ");
            x = x + y;
        }
    }
}




static void Tugas2()
{
    Console.WriteLine("---FORMAT WAKTU---");
    Console.Write("Masukan Waktu (hh:mm:sstt) : ");
    string time = Console.ReadLine();

    try
    {
        DateTime dt = DateTime.ParseExact(time, "h:mm:stt", null);
        Console.WriteLine(dt.ToString("HH:mm:ss"));
    }
    catch (Exception ex) 
    {
        Console.WriteLine($"Pesan Error : {ex.Message}");
    }

    

}


static void Tugas3()
{
    Console.WriteLine("---IMP FASHION---");
    Console.Write("Masukan Kode Baju : ");
    int kode = int.Parse(Console.ReadLine());
    Console.Write("Masukan Kode Ukuran : ");
    string size = Console.ReadLine().ToUpper();

    if (kode == 1)
    {
        Console.WriteLine("Merk Baju : IMP");
        switch (size)
        {
            case string when (size == "S"):
                Console.WriteLine("Harga : Rp. 200000");
                break;
            case string when (size == "M"):
                Console.WriteLine("Harga : Rp. 220000");
                break;
            default:
                Console.WriteLine("Harga : Rp. 250000");
                break;
        }
    }
    else if (kode == 2)
    {
        Console.WriteLine("Merk Baju : Prada");
        switch (size)
        {
            case string when (size == "S"):
                Console.WriteLine("Harga : Rp. 150000");
                break;
            case string when (size == "M"):
                Console.WriteLine("Harga : Rp. 160000");
                break;
            default:
                Console.WriteLine("Harga : Rp. 170000");
                break;
        }
    }
    else if (kode == 3)
    {
        Console.WriteLine("Merk Baju : Gucci");
        switch (size)
        {
            case string when (size == "S"):
                Console.WriteLine("Harga : Rp. 200000");
                break;
            case string when (size == "M"):
                Console.WriteLine("Harga : Rp. 200000");
                break;
            default:
                Console.WriteLine("Harga : Rp. 200000");
                break;
        }
    }
    else
    {
        Console.WriteLine("Anda Salah Memasukan Kode");
    }
}




static void Tugas4()
{
    Console.WriteLine("---BELI BAJU DAN CELANA---");
    Console.Write("Masukan Uang Anda : ");
    int uang = int.Parse(Console.ReadLine());
    Console.Write("Masukan Jumlah Baju Yang Ditulis : ");
    int n = int.Parse(Console.ReadLine());
    Console.Write("Masukan Jumlah Celana Yang Ditulis : ");
    int n2 = int.Parse(Console.ReadLine());


    int[] celana = new int[n2];
    int[] baju = new int[n];
    int[] list = new int[n*n2];
    int x = 0;

    for (int i = 0; i < n; i++)
    {
        Console.Write($"Masukan Harga Baju Ke-{i+1} : ");
        int harga = int.Parse(Console.ReadLine());
        baju[i] = harga;
    }

    for (int i = 0 ; i < n2; i++)
    {
        Console.Write($"Masukan Harga Celana Ke-{i+1} : ");
        int harga2 = int.Parse(Console.ReadLine());
        celana[i] = harga2;
    }


    for (int i = 0; i < n; i++)
    {
        for (int j = 0; j < n; j++)
        {
            if (baju[i] + celana[j] <= uang)
            {
                list[x] = baju[i] + celana[j];
                x++;
            }
    
        }
    }
    int h = list.Max();

    if (h > uang)
    {
        Console.WriteLine("Uang Anda Tidak Cukup");
    }
    else
    {
        Console.WriteLine(list.Max());
    }
}






static void Tugas5()
{
    Console.WriteLine("---ROTASI ARRAY---");
    Console.Write("Masukan Jumlah Indeks Array : ");
    int n = int.Parse(Console.ReadLine());
    Console.Write("Masukan Berapa Kali Rotasi : ");
    int rot = int.Parse(Console.ReadLine());

    int[] x = new int[n];
    int[] y = new int[n];
    int t = 0;

    for (int i = 0; i < n; i++)
    {
        Console.Write($"Masukan Value Array Indeks ke-{i} : ");
        int c = int.Parse(Console.ReadLine()); 
        x[i] = c;
    }

    for (int i = 0; i < n; i++)
    {
        if (i == 0)
        {
            Console.Write($"0 : {x[i]}, ");
        }
        else if (i == n)
        {
            Console.Write($"{x[i]}");
        }
        else
        {
            Console.Write($"{x[i]}, ");
        }
    }

    Console.WriteLine();

    for (int i = 0;i < rot; i++)
    {
        Console.Write($"{i+1} : ");
        for (int j = 0; j < n-1; j++)
        {
            y[t] = x[j+1];
            t++;
        }
        y[n-1] = x[0];
        t = 0;
        for (int r = 0; r < n; r++)
        {
            x[r] = y[r];
        }
        for (int c = 0;c < n; c++)
        {
            Console.Write($"{y[c]} ");
        }

        Console.WriteLine();
    }
}



static void Tugas6()
{
    Console.WriteLine("---SORTING---");
    Console.Write("Masukan n : ");
    int n = int.Parse(Console.ReadLine());

    int[] s = new int[n];
    int h = 0;
    for (int i = 0;i < n;i++)
    {
        Console.Write($"Masukan Angka Acak ke-{i+1} : ");
        h = int.Parse(Console.ReadLine());
        s[i] = h;
    }

    for (int i =0;i < n;i++)
    {
        for(int j = 0;j < n - i - 1;j++)
        {
            if (s[j] > s[j + 1])
            {
                int tampung = s[j];
                s[j] = s[j + 1];
                s[j + 1] = tampung;
            }
        }
    }

    Console.WriteLine("--------------------------");
    for (int i = 0; i < n;i++)
    {
        if (i == n - 1)
        {
            Console.Write($"{s[i]} ");
        }
        else
        {
            Console.Write($"{s[i]}, ");
        }
    }
}


static void Tugas7()
{
    Console.WriteLine("---POLA---");
    Console.Write("Masukan Angka Awal : ");
    int x = int.Parse(Console.ReadLine());
    Console.Write("Masukan Penambahan : ");
    int y = int.Parse(Console.ReadLine());
    Console.Write("Masukan N : ");
    int n = int.Parse(Console.ReadLine());
    Console.Write("Apakah Ada Penambahan Pola : (Y/N) ");
    string z = Console.ReadLine().ToUpper();

    if (z == "N")
    {
        for (int i = 0;i < n;i++)
        {
            if (i == 0)
            {
                Console.Write($"{x} ");
                x = x + y;
            }
            else
            {
                Console.Write($"{x} ");
                x = x + y;
            }
        }
    }
    else
    {
        for (int i = 1; i <= n; i++)
        {
            if (i == 0)
            {
                Console.Write($"{x} ");
                x = x + y;
            }
            else if (i % 3 == 0)
            {
                Console.Write($"* ");
            }
            else
            {
                Console.Write($"{x} ");
                x = x + y;
            }
        }
    }

}




//for (int i = rot;i < n; i++)
//{
//    y[t] = x[i];
//    t++;
//}
//for (int i = 0;i < rot; i++)
//{
//    y[t] = x[i];
//    t++;
//}
//for (int i = 0;i< n; i++)
//{
//    x[i] = y[i];
//}
//for (int i = 0;i < n;i++)
//{
//    Console.Write($"{x[i]} ");
//}





//static void Tugas4()
//{
//    Console.WriteLine("---BELI BAJU DAN CELANA---");
//    Console.Write("Masukan Uang Anda : ");
//    int uang = int.Parse(Console.ReadLine());

//    int[] baju = { 35, 40, 50, 20 };
//    int[] celana = { 40, 30, 45, 10 };
//    int harga = 0;
//    if (uang >= 30) {
//        if (uang < 70 && uang >= 30)
//        {
//            harga = baju[3] + celana[3];
//        }
//        else if (uang < 75 && uang >= 70)
//        {
//            harga = baju[1] + celana[1];
//        }
//        else if (uang < 95 && uang >= 75)
//        {
//            harga = baju[0] + celana[0];
//        }
//        else if (uang >= 95)
//        {
//            harga = baju[2] + celana[2];
//        }
//        Console.WriteLine($"Harga : {harga}");
//    }
//    else
//    {
//        Console.WriteLine("Uang Tidak Cukup");
//    }

//}