﻿SplitandJoin();

Console.ReadKey();


static void perulanganWhile2()
{
    bool ulangi = true;

    Console.WriteLine("---PERULANGAN WHILE 2---");
    Console.Write("Masukkan Nilai : ");
    int nilai = int.Parse(Console.ReadLine());

    while(ulangi)
    {
        Console.WriteLine($"Proses ke {nilai}");
        nilai++;

        Console.WriteLine("Ulangi Proses ? (Y/n) : ");
        string input = Console.ReadLine();
        if ( input.ToLower() == "n")
        {
            ulangi = false;
        }
    }
}


static void perulanganFile()
{
    Console.WriteLine("---PERULANGAN FILE---");
    Console.Write("Masukkan Nilai : ");
    int nilai = int.Parse(Console.ReadLine());

    while(nilai < 6)
    {
        Console.WriteLine(nilai);
        nilai++;
    }


}



static void perulanganDoWhile()
{
    Console.WriteLine("---PERULANGAN DO WHILE---");
    Console.Write("Masukkan nilai : ");
    int nilai = int.Parse(Console.ReadLine());

    do
    {
        Console.WriteLine(nilai);
        nilai++;
    } while (nilai < 6);
}


static void perulanganFor ()
{
    Console.WriteLine("---PERULANGAN FOR---");
    Console.Write("Masukkan nilai : ");
    int nilai = int.Parse (Console.ReadLine());

    for (int i = 0; i < nilai; i++)
    {
        Console.WriteLine(i);
    }

    Console.WriteLine(); //console.write("\t\n"); t : Tab, n : enter

    for (int i = nilai;i >= 0; i--)
    {
        Console.WriteLine(i);
    }
}



static void Breaks()
{
    for (int i = 0; i < 10; i++)
    {
        if (i == 2)
        {
            continue;
        }
        if (i == 6)
        {
            break;
        }
        Console.WriteLine(i);
    }

}


static void forNested()
{
    for(int i = 0;i < 3;i++)//Iterasi Baris
    {
        for(int j = 0;j < 3;j++)//Iterasi kolom
        {
            Console.Write($"({i},{j})");
        }
        Console.WriteLine() ;
    }
}



static void forEach()
{
    int[] array = { 89, 90, 90, 91, 92 };
    int sum = 0;

    foreach (int i in array)
    {
        sum += i;
    }
    Console.WriteLine($"jumlah : {sum}");

    Console.WriteLine();

    sum = 0;
    for (int i = 0; i < array.Length; i++)
    {
        sum += array[i];
    }
    Console.WriteLine($"Jumlah : {sum}");
}


static void Length()
{
    Console.WriteLine("---Length---");
    Console.Write("Masukkan Kata : ");
    string kata = Console.ReadLine();

    Console.WriteLine($"Kata {kata.ToUpper()} Mempunyai panjang karakter : {kata.Length}");
}



static void removeString()
{
    Console.WriteLine("---Remove String0");
    Console.Write("Masukkan Kata : ");
    string kata = Console.ReadLine();
    Console.Write("Masukkan Index Remove : ");
    int index = int.Parse(Console.ReadLine());

    Console.WriteLine($"{kata.Remove(index)}");
}


static void insertString()
{
    Console.WriteLine("---Insert String---");
    Console.Write("Masukkan Kata : ");
    string kata = Console.ReadLine();
    Console.Write("Masukkan Index Insert : ");
    int index = int.Parse(Console.ReadLine());
    Console.Write("Masukkan Kata Yang ingin dimasukkan : ");
    string insertKata = Console.ReadLine();

    Console.WriteLine($"{kata.Insert(index,insertKata)}");
}



static void replaceString()
{
    Console.WriteLine("---Replace String---");
    Console.Write("Masukkan Kata : ");
    string kata = Console.ReadLine();
    Console.Write("Masukkan kata yang ingin di replace : ");
    string kataLama = Console.ReadLine();
    Console.Write("Masukkan Kata baru Yang ingin direplace : ");
    string kataBaru = Console.ReadLine();

    Console.WriteLine($"{kata.Replace(kataLama, kataBaru)}");
}


static void SplitandJoin()
{
    Console.WriteLine("---SPLIT AND JOIN---");
    Console.Write("Masukkan Kalimat : ");
    string kalimat = Console.ReadLine();
    Console.Write("Masukkan Split : ");
    string split = Console.ReadLine();

    string[] katakata = kalimat.Split(split);

    foreach(string kata in katakata)
    {
        Console.WriteLine(kata);
    }

    Console.WriteLine(String.Join(" + ", katakata));

    Console.WriteLine();

    int[] deret = { 1, 2, 3, 4, 5 };
    Console.WriteLine(string.Join (" + ", deret));
}


static void subString()
{
    Console.WriteLine("---SUBSTRING---");
    Console.Write("Masukkan Kode : ");
    string kode = Console.ReadLine();
    Console.Write("Masukkan Parameter : ");
    int para1 = int.Parse(Console.ReadLine());
    Console.Write("Masukkan Parameter 2: ");
    int para2 = int.Parse(Console.ReadLine());

    if(para2 == 0)
    {
        Console.WriteLine($"Hasil SubString : {kode.Substring(para1)}");
    }
    else
    {
        Console.WriteLine($"Hasil SubString : {kode.Substring(para1, para2)}");
    }

}



static void containString()
{
    Console.WriteLine("---CONTAINS STRING---");
    Console.Write("Masukkan Kata : ");
    string kata = Console.ReadLine();
    Console.Write("Masukkan Contain : ");
    string contain = Console.ReadLine();

    if (kata.Contains(contain))
    {
        Console.WriteLine($"Kata {kata} mengandung {contain}");
    }
    else
    {
        Console.WriteLine($"Kata {kata} tidak mengandung {contain}");
    }
}




static void ToCharArray()
{
    Console.WriteLine("---STRING TO CHARARRAY---");
    Console.Write("Masukkan Kalimat : ");
    string kalimat = Console.ReadLine();

    char[] array = kalimat.ToCharArray();

    foreach ( char c in array )
    {
        Console.WriteLine(c);
    }

    Console.WriteLine();

    for(int i = 0; i < array.Length; i++)
    {
        Console.WriteLine(array[i]);
    }
}



static void convertAll()
{
    Console.WriteLine("---CONVERT ALL---");
    Console.Write("Masukkan Input Angka (pakai koma) : ");
    string[] input = Console.ReadLine().Split(",");

    int sum = 0;

    int[] array = Array.ConvertAll(input, int.Parse);//merubah array string menjadi integer

    foreach ( int i in array )
    {
        sum += i;
    }
    Console.WriteLine($"Jumlah = {sum}");
}