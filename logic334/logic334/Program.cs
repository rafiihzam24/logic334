﻿// See https://aka.ms/new-console-template for more information
// Output
Console.WriteLine("Hello, Worlds!");
Console.Write("Tulis ABCD : ");

// Input (EKSPILISIT : Tipe data selain var)
string nama = Console.ReadLine();
String kelas = "Batch 334";
int umur = 120;
bool isMR = false;

// Variabel Mutable
kelas += " nWaW";

Console.WriteLine(nama);

// Variabel Immutable
const double phi = 1.45;

//Tipe Data Var Dapat Digunakan Pada Apa Saja (IMPLISIT)
var aki = 2.114;

// Penggunaan String
Console.WriteLine("Hi, {0} Selamat Datang di {1}", nama, kelas);
Console.WriteLine("Hi, " + nama + " Selamat " + kelas + " Datang");
Console.WriteLine($"Hi, {nama} {kelas}  Selamat Datang");

Console.ReadKey();