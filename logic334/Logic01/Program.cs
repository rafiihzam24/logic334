﻿//Eksekusi Fungsi
MethodReturnType();

Console.ReadKey();



static void MethodReturnType()
{
    Console.WriteLine("---CETAK METHOD RETURN TYPE---");
    Console.Write("Masukkan Mangga : ");
    int mangga = int.Parse(Console.ReadLine());
    Console.Write("Masukkan Apel : ");
    int apel = int.Parse(Console.ReadLine());

    int jumlah = hasil(mangga, apel);

    Console.WriteLine($"Hasil mangga + apel = {jumlah}");
}

static int hasil(int mangga, int apel)
{
    int hasil = 0;

    hasil = mangga + apel;

    return hasil;//(return mangga + apel) juga bisa
}



static void OperatorLogika()
{
    Console.WriteLine("---OPERATOR LOGIKA---");
    Console.Write("Enter your age : ");
    int age = int.Parse(Console.ReadLine());
    Console.Write("Password : ");
    string password = Console.ReadLine();

    bool isAdult = age > 18;
    bool isPasswordValid = password == "admin";

    if (isAdult && isPasswordValid )
    {
        Console.WriteLine("WELCOME TO THE CLUB");
    }
    else 
    {
        Console.WriteLine("Sorry, Try Again");
    }
}



//Fungsi atau Method Perbandingan
static void OperatorPerbandingan()
{
    int mangga, apel = 0;

    Console.WriteLine("---OPERATOR PERBANDINGAN---");
    Console.Write("Masukkan Mangga : ");
    mangga = int.Parse(Console.ReadLine());
    Console.Write("Masukkan Apel : ");
    apel = int.Parse(Console.ReadLine());
    
    Console.WriteLine("Hasil Perbandingan : ");
    Console.WriteLine($"Mangga > Apel : {mangga > apel}");
    Console.WriteLine($"Mangga >= Apel : {mangga >= apel}");
    Console.WriteLine($"Mangga < Apel : {mangga < apel}");
    Console.WriteLine($"Mangga <= Apel : {mangga <= apel}");
    Console.WriteLine($"Mangga == Apel : {mangga == apel}");
    Console.WriteLine($"Mangga != Apel : {mangga != apel}");
}


//Fungsi atau Method Penambahan value
static void OperatorPenugasan()
{
    int mangga = 10;
    int apel = 8;

    //Isi Ulang Variable
    mangga += 15;

    Console.WriteLine("---OPERATOR PENUGASAN---");
    Console.Write("Masukkan Mangga : ");
    mangga = int.Parse(Console.ReadLine());
    Console.WriteLine($"Mangga : {mangga}");
    Console.Write("Masukkan Apel : ");
    apel = int.Parse(Console.ReadLine());

    //Penugasan
    apel += 5;

    Console.WriteLine($"Apel += 5 : {apel}");
}



// Fungsi Modulus
static void OperatorModulus()
{
    int mangga, orang, hasil = 0;

    Console.WriteLine("---OPERATOR MODULUS---");
    Console.Write("Masukan Mangga : ");
    mangga = int.Parse(Console.ReadLine());//Bisa Memakai Convert.ToInt atau int.Parse
    Console.Write("Masukan Orang : ");
    orang = int.Parse(Console.ReadLine());//Bisa Memakai Convert.ToInt atau int.Parse

    hasil = mangga % orang;

    Console.WriteLine($"Hasil dari modulus diatas yaitu = {hasil}");
}

// Fungsi atau Method Operator
static void OperatorAritmatika()
{
    int mangga, apel, hasil = 0;

    Console.WriteLine("---OPERATOR ARITMATIKA---");
    Console.Write("Masukan Mangga : ");
    mangga = int.Parse(Console.ReadLine());//Bisa Memakai Convert.ToInt atau int.Parse
    Console.Write("Masukan Apel : ");
    apel = int.Parse(Console.ReadLine());//Bisa Memakai Convert.ToInt atau int.Parse

    hasil = mangga + apel;

    Console.WriteLine($"Hasil dari penjumlahan diatas yaitu = {hasil}");
}




// Fungsi atau Method 
static void konversi()
{
    Console.WriteLine("---KONVERSI---");
    int myInt = Convert.ToInt32(Console.ReadLine());
    double myDouble = Convert.ToDouble(Console.ReadLine());
    bool myBool = false;

    string strMyInt = Convert.ToString(myInt);
    string strMyInt2 = myInt.ToString();
    double doubleMyInt = Convert.ToDouble(myInt);
    int intMyDouble = Convert.ToInt32(myDouble);

    Console.WriteLine(strMyInt);
    Console.WriteLine(strMyInt2);
    Console.WriteLine(doubleMyInt);
    Console.WriteLine(intMyDouble);
    Console.WriteLine(myBool.ToString());
}

