﻿using System.Runtime.InteropServices;

Tugas8();

Console.ReadKey();




static void Tugas1()
{
    Console.WriteLine("---PENILAIAN---");
    Console.Write("Nilai yang didapat : ");
    double nilai = double.Parse(Console.ReadLine());

    if (nilai >= 0 && nilai <= 100)
    {
        switch (nilai)
        {
            case double when (nilai >= 90):
                Console.WriteLine("Grade A");
                break;
            case double when (nilai < 90 && nilai >= 70):
                Console.WriteLine("Grade B");
                break;
            case double when (nilai < 70 && nilai >= 50):
                Console.WriteLine("Grade C");
                break;
            default:
                Console.WriteLine("Grade E");
                break;
        }
    } else
    {
        Console.WriteLine("Wrong Input");
    }
}



static void Tugas2()
{
    Console.WriteLine("---PULSA BONUS POINT---");
    Console.Write("Pulsa yang dibeli : ");
    int pulsa = int.Parse(Console.ReadLine());


    if (pulsa >= 10000 )
        switch (pulsa)
        {
            case int when (pulsa >= 10000 && pulsa < 25000):
                Console.WriteLine($"Pulsa :     {pulsa}");
                Console.WriteLine("Point  :     80");
                break;
            case int when (pulsa >= 25000 && pulsa < 50000):
                Console.WriteLine($"Pulsa :     {pulsa}");
                Console.WriteLine("Point  :     200");
                break;
            case int when (pulsa >= 50000 && pulsa < 100000):
                Console.WriteLine($"Pulsa :     {pulsa}");
                Console.WriteLine("Point  :     400");
                break;
            default:
                Console.WriteLine($"Pulsa :     {pulsa}");
                Console.WriteLine("Point  :     800");
                break;
        }
    else
    {
        Console.WriteLine("Tidak dapat point");
    }
}



static void Tugas3()
{
    Console.WriteLine("---PROMO GRABFOOD---");
    Console.Write("Jumlah Belanjaan : ");
    double belanja = double.Parse(Console.ReadLine());
    Console.Write("Jarak jauh : ");
    double jarak = double.Parse(Console.ReadLine());
    Console.Write("Promo : ");
    string promo = Console.ReadLine().ToUpper();
    double diskon = Math.Min(Math.Round(belanja * 40 / 100, 3), 30000);
    if (promo == "JKTOVO" && belanja >= 30000)
    {
        double ongkir = jarak * 1000;
        if (jarak <= 5 && jarak > 0)
        {
            Console.WriteLine($"Belanja : {belanja}");
            Console.WriteLine($"Diskon : {diskon}");
            Console.WriteLine($"Ongkir : 5000");
            Console.WriteLine($"Total Belanja : {(belanja - diskon) + 5000}");
        }
        else
        {
            Console.WriteLine($"Belanja : {belanja}");
            Console.WriteLine($"Diskon : {diskon}");
            Console.WriteLine($"Ongkir : {ongkir}");
            Console.WriteLine($"Total Belanja : {(belanja - diskon) + ongkir }");
        }
    }
    else
    {
        if (jarak <= 5 && jarak > 0)
        {
            Console.WriteLine($"Belanja : {belanja}");
            Console.WriteLine($"Ongkir : 5000");
            Console.WriteLine($"Total Belanja : {belanja + 5000}");
        }
        else
        {
            Console.WriteLine($"Belanja : {belanja}");
            Console.WriteLine($"Ongkir : {jarak * 1000}");
            Console.WriteLine($"Total Belanja : {belanja + (jarak * 1000)}");
        }
    }
}



static void Tugas4()
{
    Console.WriteLine("---VOUCHER SOPI---");
    Console.Write("Jumlah Belanja Anda : ");
    double belanja = double.Parse(Console.ReadLine());
    Console.Write("Ongkos Kirim : ");
    double ongkir = double.Parse(Console.ReadLine());
    Console.Write("Voucher anda : ");
    int voucher = int.Parse(Console.ReadLine());
    Console.WriteLine(" ");
    Console.WriteLine("---------------------------------------");
    Console.WriteLine(" ");

    if (voucher >= 1 && voucher <= 3 && belanja >= 30000)
    {
        switch (voucher)
        {
            case int when (voucher == 1 && belanja >= 30000):
                Console.WriteLine($"Belanja : {belanja}");
                Console.WriteLine($"Ongkos Kirim : {ongkir}");
                Console.WriteLine($"Diskon Ongkir :  {Math.Max(ongkir - 5000, 0)}");
                Console.WriteLine($"Diskon Belanja : 5000");
                Console.WriteLine($"Total Belanja : {belanja + Math.Max(ongkir - 5000, 0) - 5000}");
                break;
            case int when (voucher == 2 && belanja >= 50000):
                Console.WriteLine($"Belanja : {belanja}");
                Console.WriteLine($"Ongkos Kirim : {ongkir}");
                Console.WriteLine($"Diskon Ongkir :  {Math.Max(ongkir - 10000, 0)}");
                Console.WriteLine($"Diskon Belanja : 10000");
                Console.WriteLine($"Total Belanja : {belanja + Math.Max(ongkir - 10000, 0) - 10000}");
                break;
            case int when (voucher == 3 && belanja >= 100000):
                Console.WriteLine($"Belanja : {belanja}");
                Console.WriteLine($"Ongkos Kirim : {ongkir}");
                Console.WriteLine($"Diskon Ongkir : {Math.Max(ongkir - 10000, 0)}");
                Console.WriteLine($"Diskon Belanja : 20000");
                Console.WriteLine($"Total Belanja : {belanja + Math.Max(ongkir - 10000, 0) - 20000}");
                break;
        }
    }
    else
    {
        Console.WriteLine("Voucher atau Jumlah Belanja Tidak Memenuhi Ketentuan");
        Console.Write("Apakah anda ingin melanjutkan pembelian (Y/N) : ");
        string x = Console.ReadLine().ToUpper();
        if ( x == "Y" )
        {
            Console.WriteLine($"Belanja : {belanja}");
            Console.WriteLine($"Ongkos Kirim : {ongkir}");
            Console.WriteLine($"Total Belanja : {belanja + ongkir}");
        }
        else
        {
            Console.WriteLine("Terima Kasih Telah Menggunakan Layanan Kami");
        }
    }
}




static void Tugas5()
{
    Console.WriteLine("---ISTILAH SESUAI GENERASI---");
    Console.Write("Masukan Nama Anda : ");
    string nama = Console.ReadLine();
    Console.Write("Tahun Berapa Anda Lahir : ");
    int tahun = int.Parse(Console.ReadLine());

    if (tahun >= 1944 && tahun <= 2015)
    {
        switch (tahun)
        {
            case int when (tahun >= 1944 && tahun <= 1964):
                Console.WriteLine($"{nama}, berdasarkan tahun lahir anda tergolong generasi Baby Boomer");
                break;
            case int when (tahun >= 1965 && tahun <= 1979):
                Console.WriteLine($"{nama}, berdasarkan tahun lahir anda tergolong generasi X");
                break;
            case int when (tahun >= 1980 && tahun <= 1994):
                Console.WriteLine($"{nama}, berdasarkan tahun lahir anda tergolong generasi Y");
                break;
            case int when (tahun >= 1995 && tahun <= 2015):
                Console.WriteLine($"{nama}, berdasarkan tahun lahir anda tergolong generasi Z");
                break;
        }
    }
    else
    {
        Console.WriteLine($"{nama}, berdasarkan tahun lahir anda tergolong generasi yang belum diketahui istilahnya");
          
    }

}



static void Tugas6 ()
{
    Console.WriteLine("---SLIP GAJI---");
    Console.Write("Masukan Nama Anda : ");
    string nama = Console.ReadLine();
    Console.Write("Berapa Tunjangan Anda : ");
    int tunjangan = int.Parse(Console.ReadLine());
    Console.Write("Berapa Gaji Pokok Anda : ");
    int gapok = int.Parse(Console.ReadLine());
    Console.Write("Banyak Bulan : ");
    int bulan = int.Parse(Console.ReadLine());

    double bpjs = (3.0 / 100) * (tunjangan + gapok);
    int x = (gapok + tunjangan);

    Console.WriteLine(" ");

    if (x <= 5000000 && x > 0)
    {
        double pajak = (5.0 / 100) * x;
        Console.WriteLine($"Karyawan Atas Nama {nama} Slip Gaji Sebagai Berikut : ");
        Console.WriteLine($"Pajak  Rp.{pajak}");
        Console.WriteLine($"BPJS  Rp.{bpjs}");
        Console.WriteLine($"Gaji/bln  Rp.{(gapok + tunjangan) - (pajak + bpjs)}");
        Console.WriteLine($"Total gaji/banyak bulan  Rp.{((gapok + tunjangan) - (pajak + bpjs)) * bulan}");
    }
    else if (x > 5000000 && x <= 10000000)
    {
        double pajak = (10.0 / 100) * x;
        Console.WriteLine($"Karyawan Atas Nama {nama} Slip Gaji Sebagai Berikut : ");
        Console.WriteLine($"Pajak  Rp.{pajak}");
        Console.WriteLine($"BPJS  Rp.{bpjs}");
        Console.WriteLine($"Gaji/bln  Rp.{(gapok + tunjangan) - (pajak + bpjs)}");
        Console.WriteLine($"Total gaji/banyak bulan  Rp.{((gapok + tunjangan) - (pajak + bpjs)) * bulan}");
    }
    else if (x > 10000000)
    {
        double pajak = (15.0 / 100) * x;
        Console.WriteLine($"Karyawan Atas Nama {nama} Slip Gaji Sebagai Berikut : ");
        Console.WriteLine($"Pajak  Rp.{pajak}");
        Console.WriteLine($"BPJS  Rp.{bpjs}");
        Console.WriteLine($"Gaji/bln  Rp.{(gapok + tunjangan) - (pajak + bpjs)}");
        Console.WriteLine($"Total gaji/banyak bulan  Rp.{((gapok + tunjangan) - (pajak + bpjs)) * bulan}");
    }
    else
    {
        Console.WriteLine("Tidak Perlu Membayar Pajak");
    }
}


static void Tugas7()
{
    Console.WriteLine("---BMI INDEX---");
    Console.Write("Masukkan Berat Badan Anda : ");
    double w = double.Parse( Console.ReadLine() );
    Console.Write("Masukkan Tinggi Anda : ");
    double h = double.Parse( Console.ReadLine() );

    double bmi = Math.Round((w / h / h) * 10000, 4);

    if (bmi > 0)
    {
        switch (bmi)
        {
            case double when (bmi < 18.5):
                Console.WriteLine($"Nilai BMI anda adalah : {bmi}");
                Console.WriteLine("Anda Termasuk Kurus");
                break;
            case double when (bmi >= 18.5 && bmi < 25):
                Console.WriteLine($"Nilai BMI anda adalah : {bmi}");
                Console.WriteLine("Anda Termasuk Langsing");
                break;
            case double when (bmi >= 25):
                Console.WriteLine($"Nilai BMI anda adalah : {bmi}");
                Console.WriteLine("Anda Termasuk Gemuk");
                break;
        }
    }
    else
    {
        Console.WriteLine("Invalid");
    }
}




static void Tugas8()
{
    Console.WriteLine("---NILAI RATA RATA---");
    Console.Write("Masukan nilai MTK : ");
    int mtk = int.Parse( Console.ReadLine() );
    Console.Write("Masukan nilai Fisika : ");
    int fisika = int.Parse(Console.ReadLine());
    Console.Write("Masukan nilai Kimia : ");
    int kimia = int.Parse( Console.ReadLine() );

    int avg = (mtk + fisika + kimia) / 3;
    if (mtk <= 100 && mtk >= 0 &&  kimia <= 100 && kimia >= 0 && fisika <= 100 && fisika >= 0) 
    {
        if (avg >= 0 && avg < 50)
        {
            Console.WriteLine($"Nilai rata-rata : {avg}");
            Console.WriteLine("Maaf");
            Console.WriteLine("Kamu Gagal");
        }
        else if (avg >= 50 && avg <= 100)
        {
            Console.WriteLine($"Nilai rata-rata : {avg}");
            Console.WriteLine("Selamat");
            Console.WriteLine("Kamu Berhasil");
            Console.WriteLine("Kamu Hebat");
        }
    }
    else
    {
        Console.WriteLine("Wrong Input");
    }
}
