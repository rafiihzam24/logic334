﻿
using LogicDay4;

TimeSpan();



Console.ReadKey();

static void InitializeArray()
{
    Console.WriteLine("---INITIALIZE ARRAY---");

    //Cara Pertama
    int[] array = new int[5];

    //Cara Kedua
    int[] array2 = new int[5] {1, 2, 3, 4, 5};

    //Cara Ketiga
    int[] array3 = new int[] { 1, 2, 3, 4, 5 };

    //Cara Keempat
    int[] array4 = { 1, 2, 3, 4, 5 };

    //Cara Kelima
    int[] array5;
    array5 = new int[5] { 1,2, 3, 4, 5};


    Console.WriteLine(string.Join(" , ", array2));
    Console.WriteLine(string.Join(" , ", array3));
    Console.WriteLine(string.Join(" , ", array4));
    Console.WriteLine(string.Join(" , ", array5));
}





static void accessArray()
{
    Console.WriteLine("---AKSES ARRAY---");

    int[] intArray = new int[3];
    //Mengisi Datanya Array
    intArray[0] = 1;
    intArray[1] = 2;
    intArray[2] = 3;

    //Mengambil Data
    Console.WriteLine(intArray[0]);
    Console.WriteLine(intArray[1]);
    Console.WriteLine(intArray[2]);

    //Mengambil Data Dengan Looping
    int[] array = { 2, 3, 4, };
    for (int i = 0; i < array.Length; i++)
    {
        Console.WriteLine(array[i]);
    }


    string[] stringArray = { "Hitam", "Black", "Hideung" };
    foreach (string i in stringArray)
    {
        Console.WriteLine(i);
    }
}



static void array2Dimensi()
{
    int[,] array = new int[3, 3]
    {
        {1, 2, 3},
        { 7, 8, 9} ,
        { 10, 11, 12}
    };

    for (int i = 0;i < array.GetLength(0);i++)
    {
        for (int j = 0;j < array.GetLength(1);j++)
        {
            Console.Write((int)array[i,j] + " ");
        }
        Console.WriteLine();
    }
}



static void initializeList()
{
    Console.WriteLine("---INISIALISASI LIST---");
    List<string> list = new List<string>()
    {
        "Wilson.. Lo Siento!",
        "Adam",
        "Bill"
    };

    //Isi Data/Tambah Data
    list.Add("Broski");

    //Ubah Data
    list[3] = "ElGato";

    //Hapus Data
    list.RemoveAt(3);//Jika indeks tidak ada maka error
    list.Remove("Blud");

    Console.WriteLine(string.Join(", ", list));
}


static void panggilClass()
{
    Console.WriteLine("---PANGGIL CLASS STUDENT---");

    //Panggil Class Student
    //Student student = new Student();

    List<Student> students = new List<Student>()
    {
        new Student()
        {
            Id = 1, Name = "Wilson.. Lo Siento"
        },

        new Student()
        {
            Id = 2, Name = "Adammm"
        },

        new Student()
        {
            Id = 3, Name = "Bill"
        }
    };

    //Tambah Data
    Student student = new Student(); 
    student.Id = 4;
    student.Name = "Amigo";

    students.Add(student);

    students.Add(new Student() { Id = 5, Name = "ElGato" });//Penambahan Secara Langsung

    Console.WriteLine($"Panjang Data List Student = {students.Count}");

    foreach (Student item in students)
    {
        Console.WriteLine($"Id : {item.Id}, Name : {item.Name}");
    }

    Console.WriteLine();

    for (int i = 0; i < students.Count; i++)
    {
        Console.WriteLine($"Id : {students[i].Id}, Name : {students[i].Name}");
    }
}



static void mengaksesList()
{
    Console.WriteLine("---MENGAKSES LIST---");

    List<int> list = new List<int>();
    list.Add(1);
    list.Add(2);
    list.Add(3);

    Console.WriteLine(list[0]);
    Console.WriteLine(list[1]);
    Console.WriteLine(list[2]);

    Console.WriteLine();

    foreach (int item in list)
    {
        Console.WriteLine(item);
    }

    Console.WriteLine();

    for (int i = 0;i < list.Count;i++)
    {
        Console.WriteLine(list[i]);
    }
}


static void InsertList()
{
    Console.WriteLine("---INSERT LIST---");

    List<int> list = new List<int>();
    list.Add(1);
    list.Add(2);
    list.Add(3);

    list.Insert(1, 4);

    for (int i = 0; i < list.Count; i++)
    {
        Console.WriteLine(list[i]);
    }
}



static void indexElementList()
{
    Console.WriteLine("---INDEX ELEMENT LIST---");
    List<string> list = new List<string>();
    list.Add("1");
    list.Add("2");
    list.Add("3");

    Console.Write("Masukkan Elemen Data : ");
    string item = Console.ReadLine();

    int index = list.IndexOf(item);

    if (index != -1)
    {
        Console.WriteLine($"Element {item} is found at index {index}");
    }
    else
    {
        Console.WriteLine("Not Found");
    }
}


static void initializeDateTime()
{
    Console.WriteLine("---INISIALISASI DATE TIME---");
    
    DateTime dt1 = new DateTime();  //01.01.0001 00.00.00
    Console.WriteLine(dt1);

    DateTime dtNow = DateTime.Now; //TGL dan Waktu Hari ini
    Console.WriteLine(dtNow);

    DateTime dtNow2 = DateTime.Now; //TGL Hari ini tanpa waktu
    Console.WriteLine(dtNow2.ToString("dd/MMM/yyyy"));

    DateTime dt2 = new DateTime(2023, 11, 1);
    Console.WriteLine(dt2);

    DateTime dt3 = new DateTime(2023, 11, 2, 10, 42, 25);
    Console.WriteLine(dt3);
}



static void parsingDateTime()
{
    Console.WriteLine("---PARSING DATE TIME---");
    string dateString = "30/06/2023";
    DateTime dt1 = DateTime.Parse(dateString);

    Console.WriteLine(dt1);

    Console.WriteLine("Masukkan Datetime : (MM/dd/yyyy)");
    string dateString2 = Console.ReadLine();
    try
    {
    DateTime dt2 = DateTime.ParseExact(dateString2, "MM/d/yyyy", null);

    Console.WriteLine(dt2);
    }
    catch (Exception ex)
    {
        Console.WriteLine("Format yang anda masukkan salah ");
        Console.WriteLine("Pesan Error : " + ex.Message);
    }
}


static void dateTimeProperties()
{
    Console.WriteLine("---DATE TIME PROPERTIES---");
    DateTime date = new DateTime(2023, 11, 1, 11, 10, 25);

    int tahun = date.Year;
    int bulan = date.Month;
    int hari = date.Day;
    int jam = date.Hour;
    int min = date.Minute;
    int deok = date.Second;
    int weekday = (int)date.DayOfWeek;
    string hariString = date.DayOfWeek.ToString();
    string hariString2 = date.ToString("dddd");

    Console.WriteLine($"Tahun : {tahun}");
    Console.WriteLine($"Bulan : {bulan}");
    Console.WriteLine($"Hari : {hari}");
    Console.WriteLine($"jam : {jam}");
    Console.WriteLine($"Menit : {min}");
    Console.WriteLine($"Detik : {tahun}");
    Console.WriteLine($"Weekday : {weekday}");
    Console.WriteLine($"Hari 1 String : {hariString}");
    Console.WriteLine($"Hari 2 String : {hariString2}");
}


static void TimeSpan()
{
    Console.WriteLine("---TIME SPAN---");
    DateTime dt = new DateTime(2016, 1, 10, 11, 20, 30);
    DateTime dt2 = new DateTime(2016, 2, 20, 12, 25, 35);

    TimeSpan interval = dt2 - dt;

    Console.WriteLine("Nomer of days : " + interval.Days);
    Console.WriteLine("Total Nomer of days : " + interval.TotalDays);
    Console.WriteLine("Nomer of Hours : " + interval.Hours);
    Console.WriteLine("Total Nomer of Hours : " + interval.TotalHours);
    Console.WriteLine("Nomer of Minute : " + interval.Minutes);
    Console.WriteLine("Total Nomer of Minute : " + interval.TotalMinutes);
    Console.WriteLine("Nomer of Second : " + interval.Seconds);
    Console.WriteLine("Total Nomer of Second : " + interval.TotalSeconds);
    Console.WriteLine("Nomer of Miliseconds : " + interval.Milliseconds);
    Console.WriteLine("Nomer of Miliseconds : " + interval.TotalMilliseconds);
}