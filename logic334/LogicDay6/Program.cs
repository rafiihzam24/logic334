﻿


using LogicDay6;

Overriding();

Console.ReadKey();

static void AbstractClass()
{
    Console.WriteLine("---ABSTRAK CLASS---");
    Console.Write("Masukan Input X : ");
    int x = int.Parse(Console.ReadLine());
    Console.Write("Masukan Input Y : ");
    int y = int.Parse(Console.ReadLine());

    TesTurunan calc = new TesTurunan();
    int jumlah = calc.jumlah(x, y);
    int kurang = calc.kurang(x, y);

    Console.WriteLine($"Hasil {x} + {y} = {jumlah}");
    Console.WriteLine($"Hasil {x} + {y} = {kurang}");
}


static void ObjectClass()
{
    Console.WriteLine("---Object Class---");

    mobil Mobil = new mobil() {nama = "Ferrari", kecepatan = 0, bensin = 10, posisi = 0};
    Mobil.percepat();
    Mobil.maju();
    Mobil.isiBensin(20);

    Mobil.utama();

}

static void Constructor()
{
    Console.WriteLine("---CONSTRUCTOR---");
    mobil Mobil = new mobil("B 123 NM"); 
    string platNomor = Mobil.getPlatNomor();

    Console.WriteLine($"Mobil dengan nomor polisi : {platNomor} ");
}


static void Encapsulation()
{
    Console.WriteLine("---ENCAPSULATION---");
    PersegiPanjang pp = new PersegiPanjang();
    pp.panjang = 4.5;
    pp.lebar = 3.5;

    pp.TampilkanData();
}


static void Inheritance()
{
    Console.WriteLine("---INHERITANCE---");
    TypeMobil jenismobil = new TypeMobil();
    jenismobil.civic();
}


static void Overriding()
{
    Console.WriteLine("---OVERRIDING---");
    kucing Kucing = new kucing();
    paus Paus = new paus();

    Console.WriteLine($"Kucing {Kucing.pindah}");
    Console.WriteLine($"Paus {Paus.pindah}");
}