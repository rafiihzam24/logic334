﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LogicDay6
{
    public class Mamalia
    {
        public virtual string pindah()
        {
            return "berlari...";
        }
    }

    public class kucing : Mamalia
    {

    }

    public class paus : Mamalia
    {
        public override string pindah()
        {
            return "berenang";
        }
    }
}
