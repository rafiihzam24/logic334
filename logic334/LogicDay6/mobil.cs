﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace LogicDay6
{
    public class mobil
    {
        public double kecepatan;
        public double bensin;
        public string nama;
        public double posisi;
        public string platNomor;

        //Constructor
        public mobil(string platNomor)
        {
            this.platNomor = platNomor;
        }

        public mobil()
        {

        }

        public string getPlatNomor()
        {
            return platNomor;
        }

        //Constructor 2
        public void utama()
        {
            Console.WriteLine($"Nama : {nama} ");
            Console.WriteLine($"Plat Nomor : {platNomor}");
            Console.WriteLine($"Bensin : {bensin} ");
            Console.WriteLine($"Kecepatan : {kecepatan} ");
            Console.WriteLine($"Posisi : {posisi} ");
        }

        public void percepat()
        {
            this.kecepatan += 10;
            this.bensin += 5;
        }
        
        public void maju()
        {
            this.posisi += this.kecepatan;
            this.bensin -= 2;
        }
        public void isiBensin(double bensin)
        {
            this.bensin += bensin;
        }
    }
}
