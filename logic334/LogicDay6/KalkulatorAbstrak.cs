﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LogicDay6
{
    public abstract class KalkulatorAbstrak
    {
        public abstract int jumlah(int x, int y);
        public abstract int kurang(int x, int y);
    }

    public class TesTurunan : KalkulatorAbstrak
    {
        public override int jumlah(int x, int y)
        {
            return x + y;
    
        }
        public override int kurang(int x, int y)
        {
            return x - y;
        }
    }

}
