﻿
PadLeft();

Console.ReadKey();

static void PadLeft()
{
    Console.WriteLine("---PAD LEFT---");
    Console.Write("Masukan Input : ");
    int input = int.Parse(Console.ReadLine());
    Console.Write("Masukan Panjang Karakter : ");
    int panjang = int.Parse(Console.ReadLine());
    Console.Write("Masukan Char : ");
    char chars = char.Parse(Console.ReadLine());

    //231100001 = 2 digit tahun, 2 digit bulan, generate length

    DateTime date = DateTime.Now;

    string code = string.Empty;

    code = date.ToString("yyMM") + input.ToString().PadLeft(panjang, chars);

    Console.WriteLine($"Hasil dari Pad Left : {code}");
}



static void Rekursif()
{
    Console.WriteLine("---REKURSIF FUNCTION---");
    Console.Write("Masukan Input Awal : ");
    int awal = int.Parse(Console.ReadLine());
    Console.Write("Masukan Input Akhir : ");
    int akhir = int.Parse(Console.ReadLine());
    Console.Write("Masukan Type (ASC/DESC) : ");
    string tipe = Console.ReadLine().ToUpper();

    //Memanggil Fungsi
    Perulangan(awal, akhir, tipe);
}

static int Perulangan(int awal, int akhir, string tipe)
{
    if (tipe == "ASC")
    {
        if (awal == akhir)
        {
            Console.WriteLine(awal);
            return 0;

        }

        Console.WriteLine(awal);
        return Perulangan(awal + 1, akhir, tipe);
    } 
    else
    {
        if (awal == akhir)
        {
            Console.WriteLine(akhir);
            return 0;

        }

        Console.WriteLine(akhir);
        return Perulangan(awal, akhir - 1, tipe);
    }
}