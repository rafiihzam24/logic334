﻿Tugas5();

Console.ReadKey();



static void Tugas1()
{
    Console.WriteLine("---KELILING DAN LUAS LINGKARAN---");
    Console.Write("Berapa jari-jari lingkaran tersebut : ");
    int r = int.Parse(Console.ReadLine());
    Console.WriteLine();

    Console.Write("Keliling Lingkaran : ");
    double K = 2 * Math.PI * r;
    double doubleIntK = Convert.ToInt32(K);
    Console.WriteLine(doubleIntK+" cm");

    Console.Write("Luas Lingkaran : ");
    double L = Math.PI * r * r;
    double doubleIntL = Convert.ToInt32(L);
    Console.WriteLine(doubleIntL+" cm2");
} 



static void Tugas2()
{
    Console.WriteLine("---KELILING DAN LUAS PERSEGI---");
    Console.Write("Berapa panjang sisi dari persegi tersebut : ");
    int s = int.Parse(Console.ReadLine());

    Console.Write("Luas Persegi : ");
    int L = s * s;
    Console.WriteLine(L);

    Console.Write("Keliling Persegi : ");
    int K = 4 * s;
    Console.WriteLine(K);
}




static void Tugas3()
{
    Console.WriteLine("---HASIL MODULUS---");
    Console.Write("Angka awal yang ingin di MOD : ");
    int x = int.Parse(Console.ReadLine());
    Console.Write("Pembagi : ");
    int y = int.Parse(Console.ReadLine());
    int z = x % y;

    if (z == 0)
    {
        Console.WriteLine($"Angka {x} % {y} adalah {z}");
    }
    else
    {
        Console.WriteLine($"Angka {x} % {y} bukan 0 melainkan {z}");
    }
}



static void Tugas4()
{
    Console.WriteLine("---OTOMASI PERHITUNGAN PUNTUNG ROKOK---");
    Console.Write("Berapa puntung yang didapat : ");
    int x = int.Parse(Console.ReadLine());

    if (x >= 8)
    {
        int y = x / 8;
        Console.WriteLine($"Jumlah batang rokok yang didapat dari puntung tersebut adalah : {y}");
        int z = x % 8;
        Console.WriteLine($"Dengan sisa puntung yaitu : {z}");
        int w = y * 500;
        Console.WriteLine($"Jumlah uang yang didapat yaitu : Rp.{w}");
    }
    else
    {
        Console.WriteLine("Tidak dapat merangkai rokok");
    }
}


static void Tugas5()
{
    Console.WriteLine("---PENILAIAN OTOMATIS---");
    Console.Write("Nilai yang didapat : ");
    double x = double.Parse(Console.ReadLine());

    if (x <= 100 && x >= 0)
    {
        switch (x)
        {
            case double when (x >= 80):
                Console.WriteLine("Grade A");
                break;
            case double when (x < 80 && x >= 60):
                Console.WriteLine("Grade B");
                break;
            default:
                Console.WriteLine("Grade C");
                break;
        }
    }
    else
    {
        Console.WriteLine("Range Input is 0 - 100");
    }
}


static void Tugas6()
{
    Console.WriteLine("---GANJIL GENAP---");
    Console.Write("Angka yang ingin dicek : ");
    int x = int.Parse(Console.ReadLine());
    int y = x % 2;

    if (y == 0)
    {
        Console.WriteLine("Genap");
    }
    else
    {
        Console.WriteLine("Ganjil");
    }
}