﻿//IfStatement();
//IfElseStatement();
//IfElseIfStatement();
//IfNested();
//Ternary();
Switch();


Console.ReadKey();




static void Switch()
{
    Console.WriteLine("---SWITCH---");
    Console.Write("Buah apa yang anda pilih (Apel,Mangga,Jeruk) : ");
    string pilihan = Console.ReadLine().ToLower();

    switch(pilihan)
    {
        case "apel":
            Console.WriteLine("Anda memilih buat apel");
            break;
        case "jeruk":
            Console.WriteLine("Anda memilih buah jeruk");
            break;
        case "mangga":
            Console.WriteLine("Anda memilih buah mangga");
            break;
        default:
            Console.WriteLine("Anda memilih yang lain");
            break;
    }
}






static void Ternary ()
{
    Console.WriteLine("---TERNARY---");
    Console.Write("Masukan nilai X : ");
    int x = int.Parse(Console.ReadLine());
    Console.Write("Masukan nilai Y : ");
    int y = int.Parse(Console.ReadLine());

    if (x > y)
    {
        Console.WriteLine("X lebih besar dari Y");
    }
    else if (x < y)
    {
        Console.WriteLine("X lebih keci dari pada Y");
    }
    else
    {
        Console.WriteLine("X lebih besar atau sama dengan Y");
    }

    string z = x > y ? "X lebih besar dari Y" : x < y ? "X lebih kecil dari pada y" : "Y lebih besar atau sama dengan X";//cara ternary
    Console.WriteLine(z);
}





static void IfNested ()
{
    Console.WriteLine("---IF NESTED---");
    Console.Write("Masukan nilai anda ?!");
    int nilai = int.Parse(Console.ReadLine());

    if (nilai >= 50)
    {
        Console.WriteLine("Kamu Anjay");

        if (nilai == 100)
        {
            Console.WriteLine("Waww");
        }
    }
    else
    {
        Console.WriteLine("Kurang Beruntung");
    }

}





static void IfElseIfStatement ()
{
    Console.WriteLine("---IF ELSE IF STATeMENT");
    Console.Write("Masukkan nilai X : ");
    int x = int.Parse(Console.ReadLine());

    if (x == 10)
    {
        Console.WriteLine("X is 10");
    }
    else if (x >= 10)
    {
        Console.WriteLine("X is greater than 10");
    }
    else
    {
        Console.WriteLine("X is less than 10");
    }
}





static void IfElseStatement()
{
    Console.WriteLine("---IF ELSE STATEMENT---");
    Console.Write("Masukkan nilai X : ");
    int x = int.Parse(Console.ReadLine());

    if (x >= 10)
    {
        Console.WriteLine("X is greater than 10");
    }
    else
    {
        Console.WriteLine("X is lesser than 10");
    }
}




static void IfStatement()
{
    Console.WriteLine("---INI ADALAH IF STATEMENT---");
    Console.Write("Masukan X : ");
    int x = int.Parse(Console.ReadLine());
    Console.Write("Masukan Y : ");
    int y = int.Parse(Console.ReadLine());

    if (x >= 10)
    {
        Console.WriteLine("X is greater than or equal 10");
    };
    if (y <= 5)
    {
        Console.WriteLine("Y is lesser than or equal 5");
    };
}
