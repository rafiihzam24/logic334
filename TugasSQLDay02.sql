--TugasSQLDay02

--Database
CREATE DATABASE DB_Entertainer

--CREATE TABLE
CREATE TABLE tblArtis(
kd_artis VARCHAR(100) PRIMARY KEY,
nm_artis VARCHAR(100) not null,
jk VARCHAR(100) not null,
bayaran BIGINT not null,
award BIGINT not null,
Negara VARCHAR(100) not null
)

CREATE TABLE tblFilm(
kd_film VARCHAR(10) PRIMARY KEY,
nm_film VARCHAR(55) not null,
genre VARCHAR(55) not null,
artis VARCHAR(55) not null,
produser VARCHAR(55) not null,
pendapatan INTEGER not null,
nominasi INT not null
)

ALTER TABLE tblFilm ALTER COLUMN pendapatan BIGINT not null

CREATE TABLE tblProduser(
kd_produser VARCHAR(50) PRIMARY KEY,
nm_produser VARCHAR(50) not null,
international VARCHAR(50) not null
)

CREATE TABLE tblNegara(
kd_negara VARCHAR(100) PRIMARY KEY,
nm_negara VARCHAR(100) not null
)

CREATE TABLE tblGenre(
kd_genre VARCHAR(50) PRIMARY KEY,
nm_genre VARCHAR(50) not null
)

DROP TABLE tblFilm  

--MASUKAN DATA
INSERT INTO tblArtis(kd_artis, nm_artis, jk, bayaran, award, Negara) 
VALUES
('A001', 'ROBERT DOWNEY JR', 'PRIA', 3000000000, 2, 'AS'),
('A002', 'ANGELINA JOLIE', 'WANITA', 700000000, 1, 'AS'),
('A003', 'JACKIE CHAN', 'PRIA', 200000000, 7, 'HK'),
('A004', 'JOE TASLIM', 'PRIA', 350000000, 1, 'ID'),
('A005', 'CHELSEA ISLAN', 'WANITA', 300000000, 0, 'ID')


INSERT INTO tblFilm(kd_film, nm_film, genre, artis, produser, pendapatan, nominasi) 
VALUES
('F001', 'IRON MAN', 'G001', 'A001', 'PD01', 2000000000, 3),
('F002', 'IRON MAN 2', 'G001', 'A001', 'PD01', 1500000000, 2),
('F003', 'IRON MAN 3', 'G001', 'A001', 'PD01', 1200000000, 0),
('F004', 'AVENGER CIVIL WAR', 'G001', 'A001', 'PD01', 2000000000, 1),
('F005', 'SPIDERMAN HOME COMING', 'G001', 'A001', 'PD01', 1300000000, 0),
('F006', 'THE RAID', 'G001', 'A004', 'PD03', 800000000, 5),
('F007', 'FAST & FURIOUS', 'G001', 'A004', 'PD05', 830000000, 2),
('F008', 'HABIBIE DAN AINUN', 'G004', 'A005', 'PD03', 670000000, 4),
('F009', 'POLICE STORY', 'G001', 'A003', 'PD02', 700000000, 3),
('F010', 'POLICE STORY 2', 'G001', 'A003', 'PD02', 710000000, 1),
('F011', 'POLICE STORY 3', 'G001', 'A003', 'PD02', 615000000, 3),
('F012', 'RUSH HOUR', 'G003', 'A003', 'PD05', 695000000, 2),
('F013', 'KUNGFU PANDA', 'G003', 'A003', 'PD05', 923000000, 5)
SELECT * FROM tblFilm

UPDATE tblFilm
SET pendapatan = 1800000000 
WHERE kd_film = 'F002'

UPDATE tblFilm
SET nominasi = 0 
WHERE kd_film = 'F011'

INSERT INTO tblProduser(kd_produser, nm_produser, international) 
VALUES
('PD01', 'MARVEL', 'YA'),
('PD02', 'HONGKONG CINEMA', 'YA'),
('PD03', 'RAPI FILM', 'TIDAK'),
('PD04', 'PARKIT', 'TIDAK'),
('PD05', 'PARAMOUNT PICTURE', 'YA')

INSERT INTO tblNegara(kd_negara, nm_negara) 
VALUES
('AS', 'AMERIKA SERIKAT'),
('HK', 'HONGKONG'),
('ID', 'INDONESIA'),
('IN', 'INDIA')

INSERT INTO tblGenre(kd_genre, nm_genre) 
VALUES
('G001', 'ACTION'),
('G002', 'HORROR'),
('G003', 'COMEDY'),
('G004', 'DRAMA'),
('G005', 'THRILLER'),
('G006', 'FICTION')

SELECT * FROM tblArtis
SELECT * FROM tblFilm
SELECT * FROM tblProduser
SELECT * FROM tblNegara
SELECT * FROM tblGenre

--Soal 1
SELECT tblProduser.nm_produser, SUM(tblFilm.pendapatan) AS Pendapatan FROM tblFilm
JOIN tblProduser ON tblFilm.produser = tblProduser.kd_produser 
WHERE tblProduser.nm_produser = 'MARVEL'
GROUP BY tblProduser.nm_produser

--Soal 2
SELECT nm_film, nominasi FROM tblFilm
WHERE nominasi = '0'

--Soal 3
SELECT nm_film FROM tblFilm
--WHERE nm_film LIKE 'p%'
WHERE SUBSTRING(nm_film, 1, 1) = 'P'

--Soal 4
SELECT nm_film FROM tblFilm 
--nm_film LIKE '%y'
WHERE SUBSTRING(nm_film, DATALENGTH(nm_film), 1) = 'Y'

--Soal 5
SELECT nm_film FROM tblFilm
WHERE 
nm_film LIKE '%d%'

--Soal 6
SELECT nm_film, nm_artis FROM tblFilm
JOIN tblArtis ON tblFilm.artis = tblArtis.kd_artis

--Soal 7
SELECT nm_film, negara FROM tblFilm
JOIN tblArtis ON tblFilm.artis = tblArtis.kd_artis
JOIN tblNegara ON tblArtis.Negara = tblNegara.kd_negara
WHERE Negara = 'HK'
--WHERE Negara IN ('HK', 'IN', 'ID')

--Soal 8
SELECT nm_film, nm_negara FROM tblFilm
JOIN tblArtis ON tblFilm.artis = tblArtis.kd_artis
JOIN tblNegara ON tblArtis.Negara = tblNegara.kd_negara
WHERE nm_negara NOT LIKE '%o%'

--Soal 9
SELECT nm_artis FROM tblFilm
FULL OUTER JOIN tblArtis ON tblFilm.artis = tblArtis.kd_artis
WHERE tblArtis.kd_artis IS NULL OR tblFilm.artis is null

--Soal 10
SELECT nm_artis, nm_genre FROM tblFilm
JOIN tblArtis ON tblFilm.artis = tblArtis.kd_artis
JOIN tblGenre ON tblFilm.Genre = tblGenre.kd_genre
WHERE nm_genre = 'DRAMA'


--Soal 11
SELECT nm_artis, nm_genre FROM tblFilm
JOIN tblArtis ON tblFilm.artis = tblArtis.kd_artis
JOIN tblGenre ON tblFilm.Genre = tblGenre.kd_genre
WHERE tblGenre.nm_genre = 'ACTION' 
GROUP BY nm_genre, nm_artis

--Soal 12
SELECT kd_negara, nm_negara, COUNT(Negara) as jumlah_Film  FROM tblFilm
JOIN tblArtis ON tblFilm.artis = tblArtis.kd_artis
RIGHT JOIN tblNegara ON tblArtis.Negara = tblNegara.kd_negara
GROUP BY kd_negara, nm_negara

--Soal 13
SELECT nm_film FROM tblFilm
JOIN tblProduser ON tblFilm.produser = tblProduser.kd_produser
WHERE international = 'YA'

--Soal 14
SELECT nm_produser, COUNT(produser) as Jumlah_Film FROM tblFilm
RIGHT JOIN tblProduser ON tblFilm.produser = tblProduser.kd_produser
GROUP BY nm_produser