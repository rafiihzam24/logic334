--SQLDAY01

/*Ini adalah DBL (Data Definition Language)*/ 

--Create Database
CREATE DATABASE db_kampus

--drop database
DROP DATABASE DBPenerbit

USE db_kampus

--membuat tabel dan kolom"nya
CREATE TABLE mahasiswa(
id bigint PRIMARY KEY IDENTITY (1,1),
[name] VARCHAR(50) not null,
[address] VARCHAR(50) not null,
email VARCHAR(255) null
)

--create view
CREATE VIEW vw_mahasiswa 
AS SELECT * FROM mahasiswa

--alter
--add kolom
ALTER TABLE mahasiswa ADD nomor_hp VARCHAR(100) not null

--drop kolom
ALTER TABLE mahasiswa DROP COLUMN nomor_hp 

--alter kolom
ALTER TABLE mahasiswa ALTER COLUMN email VARCHAR(100) not null

--drop
--drop database
DROP DATABASE [nama database]

--drop table
DROP TABLE [nama table]

--drop view
DROP VIEW [nama view]

-- DML (DATA MANIPULATION LANGUAGE)


--insert
INSERT INTO mahasiswa (name, address, email) 
VALUES
('Broski', 'Brunei', 'BrostheG@gmail.com'),
('JJ', 'Jombang', 'JJjj@gmail.com'),
('dd', 'Pacitan', 'ddDD@gmail.com'),
('gg', 'Blitar', 'ggGG@gmail.com'),
('nn', 'Pamekasan', 'nnNN@gmail.com'),
('kk', 'Kolorado', 'kkKK@gmail.com')

--select
SELECT id, name, address, email FROM mahasiswa

--update
UPDATE mahasiswa SET name = 'nn november' WHERE id = 6

--delete
DELETE mahasiswa where id = 2

--create table biodata
CREATE TABLE biodata(
id bigint PRIMARY KEY IDENTITY(1,1),
mahasiswa_id bigint null,
tgl_lahir date null,
gender VARCHAR(10) null
)

--insert data
SELECT * FROM biodata

INSERT INTO biodata(mahasiswa_id, tgl_lahir, gender)
values
(1, '2020-06-10', 'Pria'),
(2, '2020-06-10', 'Pria'),
(3, '2020-06-10', 'Wanita')

--JOINS (AND, OR, NOT)
SELECT mhs.id AS id_mahasiswa, mhs.name AS nama_mahasiswa, mhs.address AS alamat, bio.tgl_lahir AS tanggal_lahir, bio.gender AS gender
FROM mahasiswa AS mhs	
JOIN biodata AS bio ON mhs.id = bio.mahasiswa_id
--WHERE mhs.id = 4 OR mhs.name = 'kk'

SELECT * FROM biodata

--order by (ASC ato DESC)
SELECT * FROM biodata ORDER BY tgl_lahir DESC, gender ASC

--top
SELECT TOP 1 * FROM biodata ORDER BY mahasiswa_id DESC

--between 
SELECT * FROM biodata WHERE mahasiswa_id BETWEEN 2 AND 3

--like 
SELECT * FROM mahasiswa 
WHERE
--name LIKE 'g%' --awalan g
--name LIKE '%k' --akhiran k
name LIKE '%g%' --mengandung g (bebas/contain)
--name LIKE '_k%' --karakter keduanya k
--name LIKE 'b__%' -- karakter pertamanya a dan length minimal 2
--name LIKE 'b%i' --awalan b dan akhiran i

--group by
SELECT name FROM mahasiswa GROUP BY name

--aggregate function (SUM, COUNT, AVG, MIN, MAX)
SELECT sum(id),name, address FROM mahasiswa GROUP BY name, address

--having
SELECT sum(id) as jumlah, name
FROM mahasiswa
WHERE name = 'kk'
GROUP BY name
--HAVING sum(id) >= 6
--ORDER BY sum(id) DESC/TOP


