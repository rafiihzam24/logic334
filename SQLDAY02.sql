--SQLDay02

SELECT * FROM mahasiswa

--in 
SELECT * FROM mahasiswa
WHERE name = 'Tungguk' or name = 'irvan'

SELECT * FROM mahasiswa
WHERE name in ('Tunggul', 'irvan')

--add column nilai pada tabel mahasiswa
ALTER TABLE mahasiswa ADD nilai INT

--update column nilai
UPDATE mahasiswa set nilai = 80 WHERE ID = 1
UPDATE mahasiswa set nilai = 80 WHERE ID = 2
UPDATE mahasiswa set nilai = 100 WHERE ID = 3
UPDATE mahasiswa set nilai = 70 WHERE ID = 4
UPDATE mahasiswa set nilai = 50 WHERE ID = 5
UPDATE mahasiswa set nilai = 40 WHERE ID = 6

--avg
SELECT AVG(nilai) FROM mahasiswa
SELECT SUM(nilai), name FROM mahasiswa
GROUP By name

--inner join atau join
SELECT * FROM mahasiswa
SELECT * FROM biodata

SELECT * FROM mahasiswa mhs
inner join biodata bio ON mhs.id = bio.mahasiswa_id 

SELECT * FROM mahasiswa mhs
left join biodata bio ON mhs.id = bio.mahasiswa_id 
WHERE bio.id IS NULL

SELECT * FROM mahasiswa mhs
left join biodata bio ON mhs.id = bio.mahasiswa_id 
WHERE bio.id IS NULL

SELECT * FROM mahasiswa mhs
right join biodata bio ON mhs.id = bio.mahasiswa_id 
WHERE bio.id IS NULL

--distinct
SELECT DISTINCT name FROM mahasiswa

--group by 
select name from mahasiswa group by name

--substring
select SUBSTRING('SQL Tutorial', 1, 3)

--charindex
select CHARINDEX('T', 'Customer')

--datalength
select DATALENGTH ('W3Schools.com')
                                                                                                                                                                                                                                                           
--case when
select name, nilai,
case
when nilai >= 80 then 'A'
when nilai >= 60 then 'B'
else 'C'
end
as grade
from mahasiswa

--concat
select CONCAT('sql', 'is', 'fuun')
select 'sql' + 'is' + 'fun'
select CONCAT ('nama : ', name) from mahasiswa

create table penjualan(
id int primary key identity (1, 1),
nameq varchar(50) not null,
harga decimal(18,0) not null
)


insert into penjualan(nameq,harga)
values
('indomie', 1500),
('closeup', 3500),
('pepsodent', 3000)

select nameq, harga, harga = 100 [ harga * 100] from penjualan

--getdate
select GETDATE()
select DAY('2023')


--datediff
select DATEDIFF(year, ' 2023-12-10', '2024-01-3')

--subquery
select * from mahasiswa mhs
left join biodata bio on mhs.id = bio.mahasiswa_id
where mhs.nilai = (select max(nilai) from mahasiswa)

--
CREATE TABLE mahasiswa_new(
id bigint PRIMARY KEY IDENTITY (1,1),
[name] VARCHAR(50) not null,
[address] VARCHAR(50) not null,
email VARCHAR(255) null
)

insert into mahasiswa_new(name, address, email)(
select name, address, email from mahasiswa
)

--create index
create index index_nameemail ON mahasiswa(name, email)

--create unique index
create unique index index_id on mahasiswa(id)

--drop index
drop index index_id on mahasiswa

--primary key
create table coba(id int not null, nama varchar(50) not null)
alter table coba add constraint pk_id primary key (id)

alter table coba drop constraint pk_id

--unique key
alter table coba add constraint unique_nama unique(nama)

alter table coba drop constraint unique_nama

--foreign key
alter table biodata 
add constraint fk_mahasiswa_id foreign key(mahasiswa_id) references mahasiswa(id)

alter table biodata drop constraint fk_mahasiswa_id
