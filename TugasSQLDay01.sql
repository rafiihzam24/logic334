--TugasSQLDay01

--CREATE DATABASE
CREATE DATABASE DBPengarang

--CREATE TABLE
CREATE TABLE tblPengarang(
ID INT PRIMARY KEY IDENTITY (1, 1),
Kd_Pengarang VARCHAR(7) not null,
Nama VARCHAR(30) not null,
Alamat VARCHAR(80) not null,
Kota VARCHAR(15) not null,
Kelamin VARCHAR(1) not null
)

--MASUKAN DATA
INSERT INTO tblPengarang(Kd_Pengarang, Nama, Alamat, Kota, Kelamin) 
VALUES
('P0001', 'Ashadi', 'Jl. Beo 25', 'Yogya','P'),
('P0002', 'Rian', 'Jl. Solo 123', 'Yogya','P'),
('P0003', 'Suwadi', 'Jl. Semangka 13', 'Bandung','P'),
('P0004', 'Siti', 'Jl. Durian 15', 'Solo','W'),
('P0005', 'Amir', 'Jl. Gajah 33', 'Kudus','P'),
('P0006', 'Suparman', 'Jl. Harimau 25', 'Jakarta','P'),
('P0007', 'Jaja', 'Jl. Singa 7', 'Bandung','P'),
('P0008', 'Saman', 'Jl. Naga 12', 'Yogya','P'),
('P0009', 'Anwar', 'Jl. Tidar 6A', 'Magelang','P'),
('P0010', 'Fatmawati', 'Jl. Renjana 4', 'Bogor','W')

--TABEL GAJI
CREATE TABLE tblGaji(
ID INT PRIMARY KEY IDENTITY (1, 1),
Kd_Pengarang VARCHAR(7) not null,
Nama VARCHAR(30) not null,
Gaji DECIMAL(18,4) not null
)

--MASUKAN DATA
INSERT INTO tblGaji(Kd_Pengarang, Nama, Gaji) 
VALUES
('P0002', 'Rian', '600000'),
('P0005', 'Amir', '700000'),
('P0004', 'Siti', '500000'),
('P0003', 'Suwadi', '1000000'),
('P0010', 'Fatmawati', '600000'),
('P0008', 'Saman', '750000')

SELECT * FROM tblPengarang
--SOAL 1
SELECT COUNT(ID) AS Jumlah
FROM tblPengarang

--SOAL 2
SELECT COUNT(ID) AS JumlahKelaminPW FROM tblPengarang
Where Kelamin = 'P' OR Kelamin = 'W'
GROUP BY Kelamin  

SELECT COUNT(ID) AS JumlahKelaminW FROM tblPengarang
Where Kelamin = 'W' 

--SOAL 3
SELECT Kota FROM tblPengarang
GROUP BY Kota

--SOAL 4
SELECT SUM(ID), COUNT(Kota), Kota FROM tblPengarang
GROUP BY Kota
HAVING COUNT(Kota) > 1


--SOAL 5
	SELECT MAX(Kd_Pengarang), MIN(Kd) FROM tblPengarang

--SOAL 6
SELECT Gaji, Nama FROM tblGaji
ORDER BY Gaji DESC

--SOAL 7
SELECT Gaji, Nama FROM tblGaji
WHERE Gaji > '600000'

--SOAL 8
SELECT SUM(Gaji) AS Jumlah FROM tblGaji

--SOAL 9
SELECT tblPengarang.Kota, SUM(Gaji) FROM tblPengarang
JOIN tblGaji ON tblPengarang.ID = tblGaji.ID
GROUP BY tblPengarang.Kota

--SOAL 10
SELECT * FROM tblPengarang
WHERE Kd_Pengarang BETWEEN 'P0003' AND 'P0006'

--SOAL 11
SELECT * FROM tblPengarang
WHERE Kota = 'Yogya' OR Kota = 'Solo' OR Kota = 'Magelang'

--SOAL 12
SELECT * FROM tblPengarang
WHERE Kota != 'Yogya'

--SOAL 13
SELECT * FROM tblPengarang
WHERE 
--Nama LIKE 'a%' 
--Nama LIKE '%i' 
--Nama LIKE '__a%'  
Nama NOT LIKE '%n'

--SOAL 14
SELECT * FROM tblPengarang
RIGHT JOIN tblGaji ON tblPengarang.Kd_Pengarang = tblGaji.Kd_Pengarang
ORDER BY tblPengarang.ID

--SOAL 15
SELECT GAJI, KOTA FROM tblGaji
LEFT JOIN tblPengarang ON tblGaji.ID = tblPengarang.ID
WHERE Gaji < 1000000
ORDER BY tblGaji.Nama

--SOAL 16
ALTER TABLE tblPengarang
ALTER COLUMN Kelamin VARCHAR(10) NOT NULL

--SOAL 17
ALTER TABLE tblPengarang
ADD Gelar VARCHAR(12) 

--SOAL 18
UPDATE tblPengarang
SET Alamat = 'Jl. Cendrawasih 65', Kota = 'Pekanbaru' 
WHERE ID = 2

--SOAL 19
CREATE VIEW vw_Pengarang
AS SELECT tblPengarang.Kd_Pengarang, tblPengarang.Nama, Kota, Gaji FROM tblPengarang
JOIN tblGaji ON tblPengarang.ID = tblGaji.ID

SELECT * FROM vw_Pengarang