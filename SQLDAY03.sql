--SQLDAY03

SELECT * FROM vw_mahasiswa

--Create Stored Procedure (SP)
CREATE PROCEDURE sp_RetrieveMahasiswa 
--Parameternya Disini (Kalau ADA)
AS
Begin
	SELECT name, address, email, nilai FROM mahasiswa
END

--RUN (Exect Procedure)
EXEC sp_RetrieveMahasiswa

--EDIT/ALTER SP
ALTER PROCEDURE sp_RetrieveMahasiswa 
--Parameternya Disini (Kalau ADA)
	@id int,
	@nama VARCHAR(50)
AS
Begin
	SELECT name, address, email, nilai FROM mahasiswa
	WHERE id = @id AND name = @nama
END

--RUN/EXEC PROCEDURE
EXEC sp_RetrieveMahasiswa 1, 'Broski'

SELECT * FROM mahasiswa


--FUNCTION
--Create Function
CREATE FUNCTION fn_total_mahasiswa
(
	@id INT
)
RETURNS INT
BEGIN
	DECLARE @hasil INT
	SELECT @hasil = COUNT(id) FROM mahasiswa WHERE id = @id
	RETURN @hasil
END

--RUN FUNCTION
SELECT dbo.fn_total_mahasiswa(1, 'Broski') 

--EDIT/ALTER FUNCTION
ALTER FUNCTION fn_total_mahasiswa
(
	@id INT,
	@nama VARCHAR(50)
)
RETURNS INT
BEGIN
	DECLARE @hasil INT
	SELECT @hasil = COUNT(id) FROM mahasiswa WHERE id = @id and name = @nama
	RETURN @hasil
END

--COBA SELECT FUNCTION MAHASISWA
SELECT mhs_id, mhs.name, dbo.fn_total_mahasiswa(mhs.id, mhs.name)
FROM mahasiswa mhs
JOIN biodata bio ON bio.mahasiswa_id = mhs.id

--Delete dan Reset Isi Table
Truncate Table [namaTABEL]