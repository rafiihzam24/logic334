--QuizSQL04

CREATE DATABASE DB_HR

CREATE TABLE tb_karyawan (
id BIGINT PRIMARY KEY IDENTITY (1,1),
nip VARCHAR(50) NOT NULL,
nama_depan VARCHAR(50) NOT NULL,
nama_belakang VARCHAR(50) NOT NULL,
jenis_kelamin VARCHAR(50) NOT NULL,
agama VARCHAR(50) NOT NULL,
tempat_lahir VARCHAR(50) NOT NULL,
tgl_lahir DATE,
alamat VARCHAR(100) NOT NULL,
pendidikan_terakhir VARCHAR(50) NOT NULL,
tgl_masuk DATE
)



CREATE TABLE tb_divisi(
id BIGINT  PRIMARY KEY IDENTITY (1,1) NOT NULL,
kd_divisi VARCHAR(50) NOT NULL,
nama_divisi VARCHAR(50) NOT NULL
)

DROP TABLE tb_divisi

CREATE TABLE tb_jabatan (
id BIGINT PRIMARY KEY IDENTITY (1,1) NOT NULL,
kd_jabatan VARCHAR(50) NOT NULL,
nama_jabatan VARCHAR(50) NOT NULL,
gaji_pokok NUMERIC,
tunjangan_jabatan NUMERIC
)


CREATE TABLE tb_pekerjaan (
id BIGINT PRIMARY KEY IDENTITY (1,1) NOT NULL,
nip VARCHAR(50) NOT NULL,
kode_jabatan VARCHAR(50) NOT NULL,
kode_divisi VARCHAR(50) NOT NULL,
tunjangan_kinerja NUMERIC,
kota_penempatan VARCHAR(50) NOT NULL
)


INSERT INTO tb_karyawan(nip, nama_depan, nama_belakang, jenis_kelamin, agama, tempat_lahir, tgl_lahir, alamat, pendidikan_terakhir, tgl_masuk)
VALUES
('001', 'Hamidi', 'Samsudin', 'Pria', 'Islam', 'Sukabumi', '1977-04-21', 'Jl. Sudirman No.12', 'S1 Teknik Mesin', '2015-12-07'),
('002', 'Paul', 'Christian', 'Pria', 'Kristen', 'Ambon', '1980-05-27', 'Jl. Veteran No.4', 'S1 Pendidikan Geografi', '2014-01-12'),
('003', 'Ghandi', 'Wamida', 'Wanita', 'Islam', 'Palu', '1992-01-12', 'Jl. Rambutan No.22', 'SMA NEGERI 02 Palu', '2014-12-01')

UPDATE tb_karyawan
SET nip = '002'
WHERE nama_depan = 'Ghandi'

select * from tb_karyawan

INSERT INTO tb_divisi(kd_divisi, nama_divisi)
VALUES
('GD', 'Gudang'),
('HRD', 'HRD'),
('KU', 'Keuangan'),
('UM', 'Umum')

INSERT INTO tb_jabatan(kd_jabatan, nama_jabatan, gaji_pokok, tunjangan_jabatan)
VALUES
('MGR', 'Manager', 5500000, 1500000),
('OB', 'Office Boy', 1900000, 200000),
('ST', 'Staff', 3000000, 750000),
('WGMR', 'Wakil Manager', 4000000, 1200000)

select * From tb_karyawan

INSERT INTO tb_pekerjaan(nip, kode_jabatan, kode_divisi, tunjangan_kinerja, kota_penempatan)
VALUES
('001', 'ST', 'KU', 750000, 'Cianjur'),
('002', 'OB', 'UM', 350000, 'Sukabumi'),
('003', 'MGR', 'HRD', 1500000, 'Sukabumi')


--SOAL 1
SELECT 
nama_depan + ' ' + nama_belakang AS Nama_Lengkap, 
nama_jabatan, tunjangan_jabatan + gaji_pokok AS Gaji_Tunjangan  FROM tb_karyawan
JOIN tb_pekerjaan ON tb_karyawan.nip = tb_pekerjaan.nip
JOIN tb_jabatan ON tb_pekerjaan.kode_jabatan = tb_jabatan.kd_jabatan
WHERE gaji_pokok + tunjangan_kinerja < 5000000

--SOAL 2
SELECT 
nama_depan + ' ' + nama_belakang AS Nama_Lengkap, 
nama_jabatan, nama_divisi, 
tunjangan_jabatan + gaji_pokok + tunjangan_kinerja AS Total_Gaji, 
(tunjangan_jabatan + gaji_pokok + tunjangan_kinerja) * 0.05 AS Pajak, 
((tunjangan_jabatan + gaji_pokok + tunjangan_kinerja) - ((tunjangan_jabatan + gaji_pokok + tunjangan_kinerja) * 0.05)) AS Gaji_Bersih FROM tb_karyawan
JOIN tb_pekerjaan ON tb_karyawan.nip = tb_pekerjaan.nip
JOIN tb_jabatan ON tb_pekerjaan.kode_jabatan = tb_jabatan.kd_jabatan
JOIN tb_divisi ON tb_pekerjaan.kode_divisi = tb_divisi.kd_divisi
WHERE jenis_kelamin = 'PRIA' AND kota_penempatan != 'Sukabumi'

--SOAL 3
SELECT 
tb_karyawan.nip, 
nama_depan + ' ' + nama_belakang AS Nama_Lengkap,
nama_jabatan, 
nama_divisi, 
((tunjangan_jabatan + gaji_pokok + tunjangan_kinerja) * 0.25) * 7 AS Bonus FROM tb_karyawan
JOIN tb_pekerjaan ON tb_karyawan.nip = tb_pekerjaan.nip
JOIN tb_jabatan ON tb_pekerjaan.kode_jabatan = tb_jabatan.kd_jabatan
JOIN tb_divisi ON tb_pekerjaan.kode_divisi = tb_divisi.kd_divisi

--SOAL 4
SELECT 
tb_karyawan.nip, 
nama_depan + ' ' + nama_belakang AS Nama_Lengkap, 
nama_jabatan, nama_divisi, 
tunjangan_jabatan + gaji_pokok + tunjangan_kinerja AS Total_Gaji, 
(tunjangan_jabatan + gaji_pokok + tunjangan_kinerja) * 0.05 AS Infak  FROM tb_karyawan
JOIN tb_pekerjaan ON tb_karyawan.nip = tb_pekerjaan.nip
JOIN tb_jabatan ON tb_pekerjaan.kode_jabatan = tb_jabatan.kd_jabatan
JOIN tb_divisi ON tb_pekerjaan.kode_divisi = tb_divisi.kd_divisi
WHERE kd_jabatan = 'MGR'

update tb_jabatan
SET nama_jabatan = 'Manager'
WHERE kd_jabatan = 'MGR'
--SOAL 5
SELECT 
tb_karyawan.nip, 
nama_depan + ' ' + nama_belakang AS Nama_Lengkap, 
nama_jabatan, pendidikan_terakhir, 2000000 AS Tunjangan_Pendidikan, 
tunjangan_jabatan + gaji_pokok + 2000000 AS Total_Gaji  FROM tb_karyawan
JOIN tb_pekerjaan ON tb_karyawan.nip = tb_pekerjaan.nip
JOIN tb_jabatan ON tb_pekerjaan.kode_jabatan = tb_jabatan.kd_jabatan
WHERE SUBSTRING(pendidikan_terakhir, 1, 2) = 'S1'
ORDER BY tb_karyawan.nip ASC

--SOAL 6
SELECT tb_karyawan.nip, nama_depan + ' ' + nama_belakang AS Nama_Lengkap, nama_jabatan, nama_divisi, 
CASE
	WHEN kd_jabatan = 'MGR' THEN ((tunjangan_jabatan + gaji_pokok + tunjangan_kinerja) * 0.25) * 7
	WHEN kd_jabatan = 'ST' THEN ((tunjangan_jabatan + gaji_pokok + tunjangan_kinerja) * 0.25) * 5
	ELSE ((tunjangan_jabatan + gaji_pokok + tunjangan_kinerja) * 0.25) * 2
END
AS
BONUS
FROM tb_karyawan
JOIN tb_pekerjaan ON tb_karyawan.nip = tb_pekerjaan.nip
JOIN tb_jabatan ON tb_pekerjaan.kode_jabatan = tb_jabatan.kd_jabatan
JOIN tb_divisi ON tb_pekerjaan.kode_divisi = tb_divisi.kd_divisi


--SOAL 7
ALTER TABLE tb_karyawan ADD CONSTRAINT unique_nip UNIQUE(nip)

--SOAL 8
CREATE INDEX index_nip ON tb_karyawan(nip)

--SOAL 9
SELECT  
CASE
	 WHEN SUBSTRING(nama_belakang, 1, 1) = 'W' THEN nama_depan + ' ' + UPPER(nama_belakang)
	 ELSE nama_depan + ' ' + nama_belakang
END
AS
Nama_Lengkap
FROM tb_karyawan

--SOAL 10
SELECT 
nama_depan + ' ' + nama_belakang AS Nama_Lengkap, 
tgl_masuk, nama_jabatan, 
nama_divisi, 
tunjangan_jabatan + gaji_pokok + tunjangan_kinerja AS Total_Gaji, 
((tunjangan_jabatan + gaji_pokok + tunjangan_kinerja) * 0.1) AS BONUS, 
FLOOR(DATEDIFF(DAY, tgl_masuk, GETDATE()) / 365.25) AS Lama_bekerja
FROM tb_karyawan
JOIN tb_pekerjaan ON tb_karyawan.nip = tb_pekerjaan.nip
JOIN tb_jabatan ON tb_pekerjaan.kode_jabatan = tb_jabatan.kd_jabatan
JOIN tb_divisi ON tb_pekerjaan.kode_divisi = tb_divisi.kd_divisi
GROUP BY nama_depan, nama_belakang, tunjangan_jabatan, tunjangan_kinerja, gaji_pokok, nama_jabatan, tgl_masuk, nama_divisi 
HAVING FLOOR(DATEDIFF(DAY, tgl_masuk, GETDATE()) / 365.25) >= 8